const camera = mp.cameras.new("gameplay");
const player = mp.players.local;
const graphics = mp.game.graphics;

const bones = [
  "door_dside_f",
  "door_pside_f",
  "door_dside_r",
  "door_pside_r",
  "bonnet",
  "boot",
];

let getDist = mp.game.gameplay.getDistanceBetweenCoords;

const degToRad = (deg) => (Math.PI * deg) / 180;

const add = (vector1, vector2) =>
  new mp.Vector3(
    vector1.x + vector2.x,
    vector1.y + vector2.y,
    vector1.z + vector2.z
  );

const sub = (vector1, vector2) =>
  new mp.Vector3(
    vector1.x - vector2.x,
    vector1.y - vector2.y,
    vector1.z - vector2.z
  );

const mulNumber = (vector1, value) =>
  new mp.Vector3(vector1.x * value, vector1.y * value, vector1.z * value);

const rotationToDirection = (rotation) => {
  let z = degToRad(rotation.z);
  let x = degToRad(rotation.x);
  let num = Math.abs(Math.cos(x));

  let result = new mp.Vector3(0, 0, 0);
  result.x = -Math.sin(z) * num;
  result.y = Math.cos(z) * num;
  result.z = Math.sin(x);

  return result;
};

const w2s = (position) => {
  let pos = graphics.world3dToScreen2d(position.x, position.y, position.z);
  if (!pos) return;

  let { x, y } = pos;

  let newPos = new mp.Vector3(0, 0, 0);
  newPos.x = (x - 0.5) * 2;
  newPos.y = (y - 0.5) * 2;
  newPos.z = 0;

  return newPos;
};

const processCoordinates = (x, y) => {
  let { x: screenX, y: screenY } = graphics.getScreenActiveResolution(0, 0);

  let relativeX = 1 - (x / screenX) * 1.0 * 2;
  let relativeY = 1 - (y / screenY) * 1.0 * 2;

  if (relativeX > 0.0) {
    relativeX = -relativeX;
  } else {
    relativeX = Math.abs(relativeX);
  }

  if (relativeY > 0.0) {
    relativeY = -relativeY;
  } else {
    relativeY = Math.abs(relativeY);
  }

  return { x: relativeX, y: relativeY };
};

function s2w(camPos, relX, relY) {
  const camRot = camera.getRot(0);
  const camForward = rotationToDirection(camRot);

  const rotUp = add(camRot, new mp.Vector3(10, 0, 0));
  const rotDown = add(camRot, new mp.Vector3(-10, 0, 0));
  const rotLeft = add(camRot, new mp.Vector3(0, 0, -10));
  const rotRight = add(camRot, new mp.Vector3(0, 0, 10));

  const camRight = sub(
    rotationToDirection(rotRight),
    rotationToDirection(rotLeft)
  );
  const camUp = sub(rotationToDirection(rotUp), rotationToDirection(rotDown));

  const rollRad = -degToRad(camRot.y);

  const camRightRoll = sub(
    mulNumber(camRight, Math.cos(rollRad)),
    mulNumber(camUp, Math.sin(rollRad))
  );
  const camUpRoll = add(
    mulNumber(camRight, Math.sin(rollRad)),
    mulNumber(camUp, Math.cos(rollRad))
  );

  const point3D = add(
    add(add(camPos, mulNumber(camForward, 10.0)), camRightRoll),
    camUpRoll
  );

  const point2D = w2s(point3D);

  if (point2D === undefined) {
    return add(camPos, mulNumber(camForward, 10.0));
  }

  const point3DZero = add(camPos, mulNumber(camForward, 10.0));
  const point2DZero = w2s(point3DZero);

  if (point2DZero === undefined) {
    return add(camPos, mulNumber(camForward, 10.0));
  }

  const eps = 0.001;

  if (
    Math.abs(point2D.x - point2DZero.x) < eps ||
    Math.abs(point2D.y - point2DZero.y) < eps
  ) {
    return add(camPos, mulNumber(camForward, 10.0));
  }

  const scaleX = (relX - point2DZero.x) / (point2D.x - point2DZero.x);
  const scaleY = (relY - point2DZero.y) / (point2D.y - point2DZero.y);
  const point3Dret = add(
    add(
      add(camPos, mulNumber(camForward, 10.0)),
      mulNumber(camRightRoll, scaleX)
    ),
    mulNumber(camUpRoll, scaleY)
  );

  return point3Dret;
}

let __pos = { x: 0, y: 0, z: 0 };
mp.events.add("render", () => {
  mp.game.graphics.drawBox(__pos.x, __pos.y, __pos.z, __pos.x + 0.05, __pos.y + 0.05, __pos.z + 0.05, 0, 255, 0, 255);
});

mp.events.add(
  "click",
  async (
    x,
    y,
    upOrDown,
    leftOrRight,
    relativeX,
    relativeY,
    worldPosition,
    hitEntity
  ) => {
    if (
      leftOrRight === "right" &&
      upOrDown === "up" &&
      typeof worldPosition === "object"
    ) {
      let camPos = camera.getCoord();
      let processedCoords = processCoordinates(x, y);
      let target = s2w(camPos, processedCoords.x, processedCoords.y);

      let dir = sub(target, camPos);
      let from = add(camPos, mulNumber(dir, 0.05));
      let to = add(camPos, mulNumber(dir, 300));

      let ray = mp.raycasting.testPointToPoint(from, to, null, 1 | 2 | 4 | 8); // 1 WORLD

      if (ray) {

        let rayPos = ray.position;
        let object = ray.entity;

        // mp.console.logInfo(`Click: ${JSON.stringify(ray.entity)}`);
        mp.console.logInfo(JSON.stringify(rayPos) + " JS");
        __pos = rayPos;
        return;

        if (!object.position) return;

        let distance = getDist(
          player.position.x,
          player.position.y,
          player.position.z,
          object.position.x,
          object.position.y,
          object.position.z,
          true
        );

        let globalDistance = getDist(
          from.x,
          from.y,
          from.z,
          rayPos.x,
          rayPos.y,
          rayPos.z,
          true
        );

        let data = new Object();

        if (object.type === "vehicle") {
          if (distance > 7) return;

          const target = getClosestBone(ray);

          let trunk = false;
          (["bonnet", "boot"]).forEach((bone) => {
            const boneIndex = object.getBoneIndexByName(bone);
            const bonePos = object.getWorldPositionOfBone(boneIndex);
            const distanceBetweenTrunk = mp.game.gameplay.getDistanceBetweenCoords(
              bonePos.x,
              bonePos.y,
              bonePos.z,
              player.position.x,
              player.position.y,
              player.position.z,
              false
            );
            mp.console.logInfo(JSON.stringify(distanceBetweenTrunk));
            if (distanceBetweenTrunk < 2) {
              trunk = true;
            }
          });

          data.vehicle = {
            remoteId: object.remoteId,
            trunk,
            type: target.id,
            doorsLocked: target.locked,
            doors: target.veh.doors,
            data: {},
            // data: await rpc.callServer("vehicleData", object.remoteId),
          };
        } else if (object.type === "player") {
          if (distance > 3) return;

          data.player = {
            _id: object.getVariable("_id"),
            id: object.id,
            name: object.name,
            deathStage: object.getVariable("deathStage"),
            cuffed: object.getVariable("cuff"),
          };
        }

        // rpc.triggerBrowsers("dispatch", {
        //   type: "HUD_SET_INTERACTION",
        //   interaction: {
        //     pos: { x, y },
        //     distance: globalDistance,
        //     ...data,
        //   },
        // });
      }
    }
  }
);

const getClosestBone = (raycast) => {
  let data = [];
  bones.forEach((bone, index) => {
    const boneIndex = raycast.entity.getBoneIndexByName(bone);
    const bonePos = raycast.entity.getWorldPositionOfBone(boneIndex);
    if (bonePos) {
      data.push({
        id: index,
        boneIndex: boneIndex,
        name: bone,
        bonePos: bonePos,
        locked:
          !raycast.entity.doors[index] ||
            (!raycast.entity.doors[index] &&
              !raycast.entity.isDoorFullyOpen(index))
            ? false
            : true,
        raycast: raycast,
        veh: raycast.entity,
        distance: mp.game.gameplay.getDistanceBetweenCoords(
          bonePos.x,
          bonePos.y,
          bonePos.z,
          raycast.position.x,
          raycast.position.y,
          raycast.position.z,
          false
        ),
        pushTime: Date.now() / 1000,
      });
    }
  });

  return data.sort((a, b) => a.distance - b.distance)[0];
};
