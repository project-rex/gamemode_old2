
using System.Globalization;
using GTANetworkAPI;
using Rex.Core.Misc;
namespace Rex.Server.Core
{
    public class EntryPoint : Script
    {
        public EntryPoint()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            Newtonsoft.Json.JsonConvert.DefaultSettings = () => JSONSettingsContainer.JSONSettings;
            RexScript.Init();
        }
    }
}
