using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

using Rex.Core.Bridge;
using Rex.Server.Core.Registrators;
using Rex.Server.Data;

namespace Rex.Server.Core
{
    public abstract class RexScript
    {
        public static ServiceCollection Services { get; private set; }

        internal static void Init()
        {
            InjectDependencies();
        }
        private static void InjectDependencies()
        {
            Services = new ServiceCollection();

            Services.AddDbContext<RexDbContext>();

            var scripts = Assembly
               .GetAssembly(typeof(RexScript))
               .GetTypes()
               .Where(x => x.IsSubclassOf(typeof(RexScript)));
            foreach (var script in scripts)
            {
                Services.AddSingleton(script);
                Debugger.DebugLog("Зарегистрирован скрипт: " + script.Name);
            }

            var provider = Services.BuildServiceProvider();
            var serverCallbackInvoker = new ServerCallbackInvoker();

            var rexCommandRegistrator = new RexCommandRegistrator();
            var serverEventRegistrator = new ServerEventRegistrator();
            var remoteEventRegistrator = new RemoteEventRegistrator();
            foreach (var service in Services)
            {
                if (service.Lifetime == ServiceLifetime.Singleton)
                {
                    object serviceInstance = provider.GetRequiredService(service.ImplementationType);
                    rexCommandRegistrator.Register(serviceInstance);
                    serverEventRegistrator.Register(serviceInstance);
                    remoteEventRegistrator.Register(serviceInstance);
                    BridgeEventRegistrator.Register(serviceInstance, Bridge.Instance);
                }
            }
        }
    }
}
