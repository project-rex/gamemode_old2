using GTANetworkAPI;
using Rex.Server.Core;

namespace Rex.Server
{
    public static class BridgeExtensions
    {
        public static void RequestClient(this Player sender, string fName, params object[] data)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.Client, fName, data);
        }
        public static void RequestClient(this Player sender, string fName)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.Client, fName);
        }

        public static void RequestCefMain(this Player sender, string fName, params object[] data)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.CEFMain, fName, data);
        }
        public static void RequestCefMain(this Player sender, string fName)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.CEFMain, fName);
        }

        public static void RequestCefDevices(this Player sender, string fName, params object[] data)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.CEFDevices, fName, data);
        }
        public static void RequestCefDevices(this Player sender, string fName)
        {
            Bridge.Request(sender, Rex.Core.Bridge.EndPoint.CEFDevices, fName);
        }
    }
}
