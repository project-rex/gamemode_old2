using System;
using System.Diagnostics;

using AspectInjector.Broker;

namespace Rex.Server.Misc
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Delegate)]
    [Aspect(Scope.Global)]
    [Injection(typeof(PerfomanceMeasureAttribute))]
    public class PerfomanceMeasureAttribute : Attribute
    {
        [Advice(Kind.Around, Targets = Target.Method)]
        public object HandleMethod(
             [Argument(Source.Name)] string name,
             [Argument(Source.Arguments)] object[] arguments,
             [Argument(Source.Target)] Func<object[], object> method)
        {
            Console.WriteLine($"Выполняем метод {name}");

            var sw = Stopwatch.StartNew();
            object result = method(arguments);
            sw.Stop();
            Console.WriteLine($"Метод {name} занял {sw.ElapsedMilliseconds} ms");
            return result;
        }
    }
}
