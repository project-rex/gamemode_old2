using GTANetworkAPI;

namespace Rex.Server
{
    public static class CefExtensions
    {
        public static void NavigateCef(this Player player, string route)
        {
            player.RequestCefMain("navigate", route);
        }
    }
}
