using System;
using System.Linq;

using GTANetworkAPI;

namespace Rex.Server.Misc
{
    public static class Utils
    {
        public static Player GetPlayerByID(int id)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (player.Value == id)
                {
                    return player;
                }
            }
            return null;
        }
        public static void SendChatMessageToPlayersInRadius(Vector3 pos, double radius, string message)
        {
            foreach (var listener in NAPI.Pools.GetAllPlayers().Where(x => GetDistance(pos, x.Position) < radius))
            {
                listener.Reply(message);
            }
        }
        public static double GetDistance(Player p1, Player p2)
        {
            var pos1 = p1.Position;
            var pos2 = p2.Position;
            return Math.Sqrt(((pos1.X - pos2.X) * (pos1.X - pos2.X)) + ((pos1.Y - pos2.Y) * (pos1.Y - pos2.Y)) + ((pos1.Z - pos2.Z) * (pos1.Z - pos2.Z)));
        }

        public static double GetDistance(Vector3 pos1, Vector3 pos2)
        {
            return Math.Sqrt(((pos1.X - pos2.X) * (pos1.X - pos2.X)) + ((pos1.Y - pos2.Y) * (pos1.Y - pos2.Y)) + ((pos1.Z - pos2.Z) * (pos1.Z - pos2.Z)));
        }
    }
}
