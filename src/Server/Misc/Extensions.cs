using GTANetworkAPI;

namespace Rex.Server
{
    public static class Extensions
    {
        public static void Reply(this Player sender, string message)
        {
            NAPI.Chat.SendChatMessageToPlayer(sender, message);
        }
    }
}
