
using Microsoft.EntityFrameworkCore;

using Rex.Domain.Models;

namespace Rex.Server.Data
{
    public class RexDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public RexDbContext() : base()
        {
            // Database.EnsureDeleted();
            // Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(RexDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Server=.\\SQLEXPRESS;Database=Rex;Integrated Security=True",
                      x => x.MigrationsAssembly(typeof(RexDbContext).Assembly.FullName)).EnableDetailedErrors().EnableSensitiveDataLogging();
        }
    }
}
