using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Rex.Domain.Models.Characters;

namespace Rex.Server.Data.TypeConfigurations
{
    internal class CharacterTypeConfiguration : IEntityTypeConfiguration<Character>
    {
        public void Configure(EntityTypeBuilder<Character> builder)
        {
            builder.ToTable("characters");

            builder.HasKey(x => x.Id);

            // builder
            //    .HasMany(c => c.Items)
            //    .WithOne();

            builder
               .HasMany(c => c.Friends)
               .WithOne();
        }
    }
}
