using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Rex.Domain.Models;
namespace Rex.Server.Data.TypeConfigurations
{
    internal class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("users");

            builder.HasKey(x => x.ScId);

            builder.HasAlternateKey(x => x.Email)
                     .HasName("UX_email_users");

            builder
               .HasMany(u => u.Characters)
               .WithOne();

            builder
               .HasMany(u => u.Bans)
               .WithOne();

            builder
               .HasMany(u => u.Warns)
               .WithOne();

            builder.Property(x => x.PasswordHash)
                 .IsRequired();
        }
    }
}
