using System;
using System.Globalization;
using System.Threading.Tasks;

using GTANetworkAPI;

using Rex.Core.Misc;
using Rex.Server.Misc;
using Rex.Domain;

namespace Rex.Server.Chat.Commands
{
    public class UtilsCommands
    {
        [RexCommand(Description = "Узнать ID игрока по никнейму")]
        public void ID(Player sender, string name)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                string plNameLower = player.Name.ToLower(CultureInfo.CurrentCulture);
                string nameLower = name.ToLower(CultureInfo.CurrentCulture);
                if (Helpers.LevenshteinDistance(nameLower, plNameLower) < 5
                    || (name.Length > 2 && plNameLower.StartsWith(nameLower, StringComparison.OrdinalIgnoreCase)))
                {
                    sender.SendChatMessage($"{player.Name} [{player.Value}]");
                }
            }
        }

        [RexCommand(Description = "Узнать ID автомобиля")]
        public void VehID(Player sender)
        {
            if (sender.IsInVehicle)
            {
                sender.SendNotification(sender.Vehicle.Handle.Value.ToString());
            }
            else
            {
                sender.SendNotification("Вы не в транспортном средстве");
            }
        }

        [RexCommand(Description = "Привязать трос к автомобилю")]
        public void TowToVehicle(Player sender, ushort vehicleID)
        {
            if (sender.IsInVehicle)
            {
                var handle = new NetHandle(vehicleID, EntityType.Vehicle);
                var vehicle = handle.Entity<Vehicle>();
                sender.TriggerEvent(nameof(TowToVehicle), vehicle);
            }
            else
            {
                sender.SendNotification("Вы не в транспортном средстве");
            }
        }

        [RexCommand(Description = "Установить автомобиль на эвакуатор")]
        public void AttachToFlatbed(Player sender, ushort carId)
        {
            if (sender.IsInVehicle)
            {
                var handle = new NetHandle(carId, EntityType.Vehicle);
                var car = handle.Entity<Vehicle>();
                // sender.Vehicle.AttachTo(flatbed, "boot", new Vector3(), new Vector3());
                sender.TriggerEvent("AttachToFlatbed", car);
            }
            else
            {
                sender.SendNotification("Вы не в транспортном средстве");
            }
        }

        [RexCommand(Description = "Привязать трос к автомобилю")]
        public static void RopeVehicle(Player sender, ushort vehicleID, string boneName1 = "bonnet", string boneName2 = "boot")
        {
            if (sender.IsInVehicle)
            {
                var handle = new NetHandle(vehicleID, EntityType.Vehicle);
                var vehicle = handle.Entity<Vehicle>();
                if (vehicle != null)
                {
                    // sender.TriggerEvent(nameof(RoadEvent.RopeVehicle), vehicle, boneName1, boneName2);
                    foreach (var p in vehicle.Occupants)
                    {
                        var pl = p as Player;
                        // pl.TriggerEvent(nameof(RoadEvent.RopeVehicle), vehicle, boneName1, boneName2);
                    }
                }
                else
                {
                    sender.Reply("Что-то идет не так.");
                }
            }
            else
            {
                sender.Reply("Вы не в транспортном средстве");
            }
        }

        [RexCommand(Description = "Буксировать самолет")]
        public void Tug(Player sender, ushort airplaneId)
        {
            if (sender.IsInVehicle)
            {
                var handle = new NetHandle(airplaneId, EntityType.Vehicle);
                var vehicle = handle.Entity<Vehicle>();
                if (vehicle != null)
                {
                    // sender.TriggerEvent(nameof(RoadEvent.TugAirplane), vehicle);
                }
            }
            else
            {
                sender.SendNotification("Вы не в транспортном средстве");
            }
        }

        [RexCommand(Description = "Убрать трос")]
        public static void RemoveRope(Player sender)
        {
            // sender.TriggerEvent(nameof(RoadEvent.RemoveRope));
        }

        [RexCommand(GreedyArg = true, Alias = "pm", Description = "Отправка приватного сообщения игроку (рекомендуется для OOC)")]
        public static void PrivateMessage(Player sender, int id, string message)
        {
            var receiver = Utils.GetPlayerByID(id);
            if (receiver != null)
            {
                message = "PM от ".WYellow() + sender.Name.WRed() + ": " + message;
                NAPI.Chat.SendChatMessageToPlayer(receiver, message, false);
            }
            else
            {
                sender.SendNotification("Игрок с указанным ID отсутсвует на севере");
            }
        }

        [RexCommand(GreedyArg = true, Description = "Посмотреть время")]
        public static void Watch(Player sender)
        {
            sender.PlayAnimation("anim@random@shop_clothes@watches", "idle_c", 49);
            Task.Delay(4300).ContinueWith((_) => sender.StopAnimation());
        }
    }
}
