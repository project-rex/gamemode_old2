using System;
using GTANetworkAPI;
using Newtonsoft.Json;
using Rex.Server.Core;

namespace Rex.Server.Chat.Commands
{
    public class AdminCommands : RexScript
    {
        Random r1;
        Random r2;
        [RexCommand(Alias = "veh", Description = "Создает транспорт [name] с цветами [color1], [color2], ливереей и экстра-модом.")]
        public void SpawnVeh(Player sender, string name, int color1 = 24, int color2 = 27, int livery = 0, int extra = 0)
        {
            r1 = new Random();
            r2 = new Random();
            var normal = sender.Position.Add(new Vector3(2, 0, 0));
            var rot = sender.Rotation;

            name = name.ToLower();
            Vehicle veh;
            if (Enum.TryParse(name, true, out VehicleHash vs2))
            {
                veh = NAPI.Vehicle.CreateVehicle(vs2, sender.Position.Add(new Vector3(2, 0, 0)), sender.Rotation, color1, color1);
            }
            else
            {
                veh = NAPI.Vehicle.CreateVehicle(NAPI.Util.GetHashKey(name), sender.Position.Add(new Vector3(2, 0, 0)), sender.Rotation.Z, color1, color1);
            }
            veh.CustomPrimaryColor = new Color(r1.Next(255), r2.Next(255), r1.Next(255));
            veh.CustomSecondaryColor = new Color(r2.Next(255), r1.Next(255), r2.Next(255));
            sender.SetIntoVehicle(veh, -1);
            sender.SendChatMessage($"Транспорт {name} создан");
            NAPI.Vehicle.SetVehicleLivery(veh, livery);
            NAPI.Vehicle.SetVehicleExtra(veh, extra, true);
        }
        [RexCommand(Alias = "pos", Description = "Отображает позицию Вас/игрока [playerId]")]
        public void GetPosition(Player sender, int playerId = -1)
        {
            if (playerId == -1)
            {
                Debugger.DebugLog(JsonConvert.SerializeObject(sender.Position));
            }
            else
            {
                var targetPlayer = Misc.Utils.GetPlayerByID(playerId);
                if (targetPlayer == null)
                {
                    Debugger.DebugLog(JsonConvert.SerializeObject(sender.Position));
                }
                else
                {
                    sender.SendChatMessage($"Игрока с таким ID нет");
                }
            }
        }
    }
}
