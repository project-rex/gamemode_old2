using System;
using System.Collections.Generic;
using GTANetworkAPI;

using Rex.Domain;
using Rex.Server.Core;
using Rex.Server.Misc;

namespace Rex.Server.Chat.Commands
{
    public class RPCommands : RexScript
    {
        [RexCommand("b", GreedyArg = true)]
        public static void B(Player player, string text)
        {
            string msg = ("(( " + player.Name + ": " + text + " ))").Grey();
            var playersInRadius = NAPI.Player.GetPlayersInRadiusOfPlayer(25, player);
            foreach (var pl in playersInRadius)
            {
                pl.SendChatMessage(msg);
            }
        }

        [RexCommand("try", GreedyArg = true)]
        public static void Try(Player player, string action)
        {
            var random = new Random();
            int n = random.Next(0, 2);
            string msg = (player.Name + " попытался " + action).Pink();

            if (n == 0)
            {
                msg += " | Неудачно".Red();
            }
            else
            {
                msg += " | Удачно".Green();
            }

            var playersInRadius = NAPI.Player.GetPlayersInRadiusOfPlayer(25, player);
            foreach (var pl in playersInRadius)
            {
                pl.SendChatMessage(msg);
            }
        }

        [RexCommand("me", GreedyArg = true)]
        public static void Me(Player player, string action)
        {
            string msg = (player.Name + " " + action).WPink();
            var playersInRadius = NAPI.Player.GetPlayersInRadiusOfPlayer(25, player);
            foreach (var pl in playersInRadius)
            {
                pl.SendChatMessage(msg);
            }
        }

        [RexCommand("do", GreedyArg = true)]
        public static void Do(Player player, string action)
        {
            string msg = $"{action.WOrange()}" + $" (({player.Name}))".WWhite();
            var playersInRadius = NAPI.Player.GetPlayersInRadiusOfPlayer(30, player);
            foreach (var pl in playersInRadius)
            {
                pl.SendChatMessage(msg);
            }
        }

        [RexCommand("anim", GreedyArg = true)]
        public static void Animlist(Player player, string dict, string animation)
        {
            player.PlayAnimation(dict, animation, 0);
        }

        [ServerEvent(Event.ChatMessage)]
        public static void OnChatMessage(Player player, string message)
        {
            string output = player.Name + ": " + message;
            double radius = 100;
            if (message.StartsWith("/me", StringComparison.CurrentCulture))
            {
                output = output.Purple();
                radius = 45;
            }
            Utils.SendChatMessageToPlayersInRadius(player.Position, radius, output);
        }
    }
}
