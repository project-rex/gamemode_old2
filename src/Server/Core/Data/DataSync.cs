﻿using System.Linq;
using GTANetworkAPI;
using Rex.Core.Bridge;
using Rex.Core.Data;
using Rex.Domain.Models.Characters;
using Rex.Domain.Models.Vehicles;

namespace Rex.Server.Core.Data
{
    class DataSync : RexScript
    {
        readonly DataController controller;
        public DataSync()
        {
            controller = new DataController(Debugger.Instance);
        }
        public static NotifyPropertyChangedBase GetEntitySharedSkeleton(NotifyPropertyChangedBase c)
        {
            // foreach (var property in c.GetType().GetProperties())
            // {
            //     if (!property.GetCustomAttributes(typeof(SyncableAttribute), false).Any())
            //     {
            //         property.SetValue(c, null);
            //     }
            // }
            return c;
        }
        [ServerEvent(Event.EntityCreated)]
        public static void OnEnityCreated(Entity e)
        {
            if (e is Vehicle)
            {
                var carInitialData = new Car()
                {
                    NumberPlate = "Rex228"
                };
                for (int i = 0; i < 4; i++)
                {
                    carInitialData.Windows.Add(new Window(i));
                }
                e.SetData("State", carInitialData);
            }
            else if (e is Player)
            {
                var plInitialData = new Character()
                {
                    Name = "Player227"
                };
                e.SetData("State", plInitialData);
            }
        }

        public static Entity GetEntityByTypeAndRemoteId(int entityType, int remoteId)
        {
            Entity entity = default;
            var type = (EntityType)entityType;
            switch (type)
            {
                case EntityType.Player:
                    entity = NAPI.Pools.GetAllPlayers().Find(x => x.Id == remoteId);
                    break;
                case EntityType.Vehicle:
                    entity = NAPI.Pools.GetAllVehicles().Find(x => x.Id == remoteId);
                    break;
                default:
                    break;
            }
            return entity;
        }
        [RexEvent(EndPoint.Client)]
        public static NotifyPropertyChangedBase GetEntitySyncData(Player sender, int entityType, int remoteId)
        {
            var entity = GetEntityByTypeAndRemoteId(entityType, remoteId);
            return GetEntitySharedSkeleton(entity.GetData<NotifyPropertyChangedBase>("State"));
        }

        [RemoteEvent("CommitEntityData")]
        public void CommitEntityData(Player player, int entityType, int remoteId, EntityChangedEventArgs args)
        {
            var entity = GetEntityByTypeAndRemoteId(entityType, remoteId);
            var state = entity.GetData<NotifyPropertyChangedBase>("State");
            controller.CommitChange(state, args);
            Bridge.RequestAllInRange(player.Position, 10000, EndPoint.Client, "ApplySyncData", entityType, entity.Id, args);
        }
    }
}
