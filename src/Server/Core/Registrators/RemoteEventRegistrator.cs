using System.Collections.Generic;
using System.Reflection;

using GTANetworkAPI;

using Rex.Core.Misc;

namespace Rex.Server.Core.Registrators
{
    public class RemoteEventRegistrator : IRegistrator
    {
        public void Register(object serviceInstance)
        {
            var methods = Helpers.GetTypeMethodsWithAttribute(serviceInstance.GetType(), typeof(RemoteEventAttribute));
            foreach (var m in methods)
            {
                var attr = (RemoteEventAttribute)m.GetCustomAttribute(typeof(RemoteEventAttribute));
                NAPI.ClientEvent.Register(m, attr.RemoteEventString ?? m.Name, serviceInstance);
            }
        }
    }
}
