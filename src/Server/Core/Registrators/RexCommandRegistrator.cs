using System.Collections.Generic;
using System.Reflection;

using GTANetworkAPI;

using Rex.Core.Misc;
using Rex.Server.Core.Commands;

namespace Rex.Server.Core.Registrators
{
    public class RexCommandRegistrator: IRegistrator
    {
        public static List<RexCommandInfo> Commands { get; set; } = new List<RexCommandInfo>();

        public void Register(object serviceInstance)
        {
            var methods = Helpers.GetTypeMethodsWithAttribute(serviceInstance.GetType(), typeof(RexCommandAttribute));
            foreach (var m in methods)
            {
                var attr = (RexCommandAttribute)m.GetCustomAttribute(typeof(RexCommandAttribute));
                attr.Alias ??= m.Name;
                NAPI.Command.Register(m, new RuntimeCommandInfo()
                {
                    Alias = attr.Alias,
                    ClassInstance = serviceInstance,
                    Description = attr.Description,
                    GreedyArg = attr.GreedyArg,
                    SensitiveInfo = attr.SensitiveInfo,
                    Hide = attr.Hide,
                });
                Commands.Add(new RexCommandInfo(attr, m));
            }
        }
    }
}
