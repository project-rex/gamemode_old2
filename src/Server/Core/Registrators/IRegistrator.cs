namespace Rex.Server.Core.Registrators
{
    public interface IRegistrator
    {
        void Register(object serviceInstance);
    }
}
