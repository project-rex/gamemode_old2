using System.Collections.Generic;
using System.Reflection;

using GTANetworkAPI;

using Rex.Core.Misc;

namespace Rex.Server.Core.Registrators
{
    public class ServerEventRegistrator : Script, IRegistrator
    {
        private static readonly Dictionary<Event, List<EventRegistrationInfo>> _serverEvents = new();
        public void Register(object serviceInstance)
        {
            foreach (var method in Helpers.GetTypeMethodsWithAttribute(serviceInstance.GetType(), typeof(ServerEventAttribute)))
            {
                var attribute = (ServerEventAttribute)method.GetCustomAttribute(typeof(ServerEventAttribute));
                var eventRegistation = new EventRegistrationInfo(method, serviceInstance);
                if (!_serverEvents.ContainsKey(attribute.EventId))
                {
                    _serverEvents.Add(attribute.EventId, new List<EventRegistrationInfo> { eventRegistation });
                }
                else
                {
                    _serverEvents[attribute.EventId].Add(eventRegistation);
                }
            }
        }
        private static void CallServerEvents(Event e, params object[] args)
        {
            if (_serverEvents.ContainsKey(e))
            {
                foreach (var regInfo in _serverEvents[e])
                {
                    regInfo.MethodInfo.Invoke(regInfo.Target, args);
                }
            }
        }
        // Events
        [ServerEvent(Event.ResourceStart)]
        public static void OnResourceStart()
        {
            CallServerEvents(Event.ResourceStart);
        }
        [ServerEvent(Event.EntityCreated)]
        public static void OnEntityCreated(Entity e)
        {
            CallServerEvents(Event.EntityCreated, e);
        }
        [ServerEvent(Event.PlayerConnected)]
        public static void OnPlayerConnected(Entity e)
        {
            CallServerEvents(Event.PlayerConnected, e);
        }
        [ServerEvent(Event.PlayerDisconnected)]
        public static void OnPlayerDisconnected(Player p, DisconnectionType type, string reason)
        {
            CallServerEvents(Event.PlayerDisconnected, p, type, reason);
        }
    }
}
