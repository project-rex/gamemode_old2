using System;
using System.Reflection;
using System.Threading.Tasks;

using GTANetworkAPI;

using Rex.Core.Bridge;
using Rex.Core.Misc;

namespace Rex.Server.Core
{
    public class ServerCallbackInvoker : ICallbackInvoker
    {
        private static object[] MergeParams(object[] data, ParameterInfo[] ps, object sender)
        {
            var player = (Player)sender;
            object[] result = Array.Empty<object>();
            if (ps.Length > 0)
            {
                if (ps[0].ParameterType == typeof(Player))
                {
                    var psWithoutPlayer = new ParameterInfo[ps.Length - 1];
                    Array.Copy(ps, 1, psWithoutPlayer, 0, psWithoutPlayer.Length);
                    var parsedParams = BridgeArgumentParser.ConvertArgsToParameters(data, psWithoutPlayer);
                    object[] _parsedParams = new object[parsedParams.Length + 1];
                    Array.Copy(parsedParams, 0, _parsedParams, 1, parsedParams.Length);
                    _parsedParams[0] = player;
                    result = _parsedParams;
                }
                else
                {
                    result = BridgeArgumentParser.ConvertArgsToParameters(data, ps);
                }
            }
            return result;
        }
        public void InvokeAction(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parsedParams = MergeParams(msgData, eventInfo.Callback.GetParameters(), sender);
            if (eventInfo.Optimize)
            {
                var actionExpr = FastExpressionInvoker.GetActionExpression(eventInfo.Callback);
                actionExpr.Invoke(eventInfo.Target, parsedParams);
            }
            else
            {
                eventInfo.Callback.Invoke(eventInfo.Target, parsedParams);
            }
        }

        public object InvokeHandler(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parsedParams = MergeParams(msgData, eventInfo.Callback.GetParameters(), sender);
            if (eventInfo.Optimize)
            {
                var actionExpr = FastExpressionInvoker.GetFuncExpression(eventInfo.Callback);
                return actionExpr.Invoke(eventInfo.Target, parsedParams);
            }
            else
            {
                return eventInfo.Callback.Invoke(eventInfo.Target, parsedParams);
            }
        }
        public Task<object> InvokeHandlerAsync(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parsedParams = MergeParams(msgData, eventInfo.Callback.GetParameters(), sender);
            if (eventInfo.Optimize)
            {
                var actionExpr = FastExpressionInvoker.GetFuncExpression(eventInfo.Callback);
                return (Task<object>)actionExpr.Invoke(eventInfo.Target, parsedParams);
            }
            return (Task<object>)eventInfo.Callback.Invoke(eventInfo.Target, parsedParams);
        }
    }
}
