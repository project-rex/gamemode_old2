using System;
using System.Linq;
using System.Threading.Tasks;

using GTANetworkAPI;

using Rex.Core.Bridge;
using Rex.Core.Misc;

namespace Rex.Server.Core
{
    public sealed class Bridge : BridgeBase
    {
        private static Bridge s_instance;
        public static Bridge Instance
        {
            get
            {
                s_instance ??= new Bridge();
                return s_instance;
            }
        }

        public override EndPoint CurrentEndPoint { get; } = EndPoint.Server;
        protected override ICallbackInvoker CBInvoker { get; } = new ServerCallbackInvoker();
        protected override IDebugger Logger { get; } = new Debugger();

        protected override void SendMessage(Packet message, object receiver)
        {
            var playerReceiver = receiver as Player;
            if (playerReceiver != null)
            {
                NAPI.ClientEvent.TriggerClientEvent(playerReceiver, nameof(Packet), message);
            }
            else
            {
                Logger.DebugWarn("Warning: Player was not found for response");
            }
        }
        public static void BroadcastClient(Packet message)
        {
            if (message.Destination == Instance.CurrentEndPoint)
            {
                throw new Exception("WTF");
            }
            NAPI.ClientEvent.TriggerClientEventForAll(nameof(Packet), message);
        }

        public static void Request(Player sender, EndPoint point, string fName, params object[] data)
        {
            Instance.InternalRequest(sender, point, fName, data);
        }
        public static void RequestAllInRange(Vector3 location, float range, EndPoint point, string fName, params object[] data)
        {
            var nearByPlayers = NAPI.Pools.GetAllPlayers().Where(x => x.Position.DistanceTo(location) < range);
            foreach (var pl in nearByPlayers)
            {
                Instance.InternalRequest(pl, point, fName, data);
            }

        }
        public static Task<T> RequestAsync<T>(Player sender, EndPoint point, string fName, params object[] data)
        {
            return Instance.InternalRequestAsync<T>(sender, point, fName, data);
        }
    }
}
