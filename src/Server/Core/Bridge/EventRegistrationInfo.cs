using System.Reflection;

namespace Rex.Server.Core
{
    public class EventRegistrationInfo
    {
        public MethodInfo MethodInfo { get; set; }
        public object Target { get; set; }

        public EventRegistrationInfo(MethodInfo methodInfo, object target)
        {
            MethodInfo = methodInfo;
            Target = target;
        }
    }
}
