using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using GTANetworkAPI;

namespace Rex.Server.Core.Commands
{
    public class RexCommandInfo
    {
        public string Alias { get; set; }
        public string Description { get; set; }
        public IEnumerable<CommandParameter> Parameters { get; set; }

        public RexCommandInfo(RexCommandAttribute attribute, MethodInfo method)
        {
            Alias = attribute.Alias;
            Description = attribute.Description;

            Parameters = method
            .GetParameters()
            .Where(x => x.ParameterType != typeof(Player))
            .Select(p => new CommandParameter()
            {
                Name = p.Name,
                Required = !p.IsOptional,
                Type = p.ParameterType.Name.ToLower()
            });
        }
        public class CommandParameter
        {
            public string Name { get; set; }

            public string Type { get; set; }

            public bool Required { get; set; }
        }
    }
}
