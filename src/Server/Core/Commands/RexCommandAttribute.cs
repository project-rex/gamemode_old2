using System;
using System.Reflection;

using AspectInjector.Broker;

using GTANetworkAPI;

using Rex.Domain.Models;

namespace Rex.Server
{
    [Aspect(Scope.Global)]
    [Injection(typeof(RexCommandAttribute))]
    public class RexCommandAttribute : CommandAttribute
    {
        public Role Role { get; set; } = Role.Player;

        /// <summary>
        /// Команда без слеша
        /// </summary>
        public RexCommandAttribute()
        {
        }
        /// <summary>
        /// Команда без слеша
        /// </summary>
        /// <param name="alias">Команда без слеша</param>
        public RexCommandAttribute(string alias)
        {
            Alias = alias;
        }

        [Advice(Kind.Around, Targets = Target.Method)]
        public object CheckAccess(
             [Argument(Source.Target)] Func<object[], object> target,
             [Argument(Source.Metadata)] MethodBase mb,
             [Argument(Source.Arguments)] object[] args)
        {
            var attr = (RexCommandAttribute)mb.GetCustomAttribute(typeof(RexCommandAttribute));
            var pl = (Player)args[0];
            // var user = pl.User();
            // if (user?.Role < attr.Role)
            // {
            // 	// pl.SetAlert("warning", $"Вам недостуна эта команда {user?.Role} < {attr.Role}");
            // 	return null;
            // }
            return target.Invoke(args);
        }
    }
}
