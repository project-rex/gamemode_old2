using Rex.Core.Misc;

using Color = System.Drawing.Color;
using Console = Colorful.Console;
namespace Rex.Server
{
    public sealed class Debugger : IDebugger
    {
        private static Debugger debugger;
        public static IDebugger Instance
        {
            get
            {
                if (debugger == null)
                {
                    debugger = new Debugger();
                }
                return debugger;
            }
        }
        public void DebugLog(string message)
        {
            Console.WriteLine($"[{System.DateTime.Now.ToShortTimeString()}] {message}", Color.LightBlue);
        }
        public void DebugWarn(string message)
        {
            Console.WriteLine($"[{System.DateTime.Now.ToShortTimeString()}] {message}", Color.OrangeRed);
        }
        public void DebugError(string message)
        {
            Console.WriteLine($"[{System.DateTime.Now.ToShortTimeString()}] {message}", Color.DarkRed);
        }

        public static void DebugLog(object message)
        {
            Instance.DebugLog(message?.ToString());
        }
        public static void DebugWarn(object message)
        {
            Instance.DebugWarn(message?.ToString());
        }
        public static void DebugError(object message)
        {
            Instance.DebugError(message?.ToString());
        }
    }
}
