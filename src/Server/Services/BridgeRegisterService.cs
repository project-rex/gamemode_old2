using GTANetworkAPI;

using Rex.Core.Bridge;

namespace Rex.Server.Core.Registrators
{
    public class BridgeService : RexScript
    {
        [RemoteEvent(nameof(Packet))]
        public void OnPacketReceived(Player player, Packet msg)
        {
            Bridge.Instance.OnMessageReceived(msg, player);
        }
    }
}
