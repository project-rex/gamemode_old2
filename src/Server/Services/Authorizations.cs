using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GTANetworkAPI;
using Rex.Core.Bridge;
using Rex.Core.Data;
using Rex.Domain.Models;
using Rex.Domain.Models.Characters;
using Rex.Domain.Models.Penalties;
using Rex.Server.Core;
using Rex.Server.Data;

namespace Rex.Server.Services
{
    public static class PlayerMethods
    {
        public static User Db(this Player player)
        {
            return Players.Authorized[player];
        }
    }

    public static class Players
    {
        public static Dictionary<Player, User> Authorized = new Dictionary<Player, User>();
    }

    public class Authorizations : RexScript
    {
        RexDbContext dbContext;

        public Authorizations(RexDbContext context)
        {
            dbContext = context;
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Player p, DisconnectionType type, string reason)
        {
            Debugger.DebugLog("Player disconnected " + p.SocialClubId + " " + p.SocialClubName);
            if (Players.Authorized.Remove(p))
            {
                // set position && save
            }
        }

        [RexEvent(EndPoint.Client)]
        public async void OnPlayerReady(Player p)
        {
            var user = await dbContext.Users.FindAsync(p.SocialClubId);
            if (user == null)
            {
                Debugger.DebugLog("Welcome new player with ScId: " + p.SocialClubId + " " + p.SocialClubName);

                user = new Domain.Models.User()
                {
                    ScId = p.SocialClubId,
                    Warns = new List<Warn>(),
                    Bans = new List<Ban>(),
                    Characters = new SyncableCollection<Character>(),
                    DonateBalance = 0.0m,
                    Email = "",
                    LastSessions = new List<Session>(),
                    PasswordHash = "",
                    Permissions = Domain.Models.AccountPermissions.Player,
                    RegisterSession = new Session()
                    {
                        Id = new Guid(),
                        IP = p.Address,
                        Serial = p.Serial,
                        Time = new DateTime()
                    }
                };

                await dbContext.AddAsync<User>(user);
                await dbContext.SaveChangesAsync();
                Console.WriteLine("Player created");
            }
            else
            {
                Debugger.DebugLog("Player connected: " + p.SocialClubId + " " + p.SocialClubName);

                if (Players.Authorized.ContainsValue(user))
                {
                    foreach (var pair in Players.Authorized)
                    {
                        if (pair.Value == user)
                        {
                            pair.Key.Kick("Авторизация с другого клиента");
                            break;
                        }
                    }

                    Debugger.DebugLog(p.SocialClubId + "is already connected");
                }
            }

            if (Players.Authorized.TryAdd(p, user))
            {
                p.NavigateCef("hud");
            }
            else
            {
                // setAlert ( already auth )
            }
        }
    }
}
