using System;

using GTANetworkAPI;
using Rex.Server.Core;

namespace Rex.Server.Services.Tests
{
    public class ChatTest : RexScript
    {
        [RexCommand("flood")]
        public static void Flood(Player sender, int times)
        {
            for (int i = 0; i < times; i++)
            {
                sender.Reply("Hello, " + sender.Name + " " + times + Guid.NewGuid().ToString()[times..] + Guid.NewGuid().ToString()[times..]);
            }
        }
    }
}
