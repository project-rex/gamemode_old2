
using GTANetworkAPI;

using Rex.Core.Bridge;
using Rex.Server.Core;
using Rex.Server.Data;
namespace Rex.Server.Services.Tests
{
    public class BridgeTest : RexScript
    {
        private RexDbContext dbContext;

        public BridgeTest(RexDbContext context)
        {
            dbContext = context;
        }

        [RexEvent(EndPoint.Client)]
        public static void TestBridgeClientToServerAction(Player sender, int testVar)
        {
            sender.SendChatMessage("Server: " + testVar.ToString());
        }

        [RexCommand("tbc2")]
        public static void TestBridgeServerToClientAction(Player sender, int testVar)
        {
            Bridge.Request(sender, EndPoint.Client, "TestBridgeServerToClientAction", testVar);
        }

        [RexEvent(EndPoint.Client)]
        public static int TestBridgeClientToServerHandler(Player sender, int r)
        {
            sender.SendChatMessage("SERVER [TBC3]: " + r);
            return r;
        }

        [RexCommand("tbc4")]
        public static async void TestBridgeServerToClientHandler(Player sender, int r)
        {
            int result = await Bridge.RequestAsync<int>(sender, EndPoint.Client, "TestBridgeServerToClientHandler", r);
            Debugger.DebugLog("Клиент по запросу вернул: " + result);
        }
    }
}
