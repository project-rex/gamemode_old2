using System;

using GTANetworkAPI;

using Rex.Core.Bridge;
using Rex.Domain.Models.Characters;
using Rex.Server.Core;
namespace Rex.Server.Services.Tests
{
    public class IndicatorsTest : RexScript
    {
        [RexCommand("test_status")]
        public void SetStatus(Player sender)
        {
            var random = new Random();
            var status = new Status()
            {
                Death = random.Next(0, 100),
            };
            Bridge.Request(sender, EndPoint.CEFMain, "updateIndicators", status);

            NAPI.Task.Run(() =>
            {
                status = new Status()
                {
                    Radiation = random.Next(0, 100),
                    Stamina = random.Next(0, 100),
                    Vest = random.Next(0, 100),
                };
                Bridge.Request(sender, EndPoint.CEFMain, "updateIndicators", status);
            }, 3000);
        }
    }
}
