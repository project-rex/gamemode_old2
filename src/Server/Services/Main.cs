
using GTANetworkAPI;
using Rex.Core.Bridge;
using Rex.Core.Data;

using Rex.Server.Core;
using Rex.Server.Data;

namespace Rex.Server.Services
{
    public class Main : RexScript
    {
        RexDbContext dbContext;
        public Main(RexDbContext context)
        {
            dbContext = context;
        }

        [RexEvent(EndPoint.Client)]
        public INotifyEntityChangedBase WarmUp(Player p, INotifyEntityChangedBase entityType)
        {
            return entityType;
        }
    }
}
