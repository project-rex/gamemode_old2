using System;
using System.Collections.Generic;
using Rex.Client.Core;
using Rex.Client.UI;
using Rex.Core.Bridge;
using Rex.Core.Data;

namespace Rex.Client
{
    public class EntryPoint : RexScript
    {
        public EntryPoint()
        {
            RAGE.Events.OnPlayerReady += () =>
            {
                if (!CEFManager.WindowsReady)
                {
                    CEFManager.OnCEFReady += OnSystemsReady;
                }
                else
                {
                    OnSystemsReady();
                }
            };
            RAGE.Events.Tick += Tick;
        }
        public void WarmupBridge()
        {
            var entity = RAGE.Elements.Player.LocalPlayer;
            Debugger.DebugLog("Warming bridge...");
            foreach (var item in Rex.Core.DomainTypesContainer.DomainTypes)
            {
                if (typeof(INotifyEntityChangedBase).IsAssignableFrom(item) && item.GetConstructor(Type.EmptyTypes) != null)
                {
                    Debugger.DebugLog(item);
                    Bridge.Request(EndPoint.Server, "WarmUp", (arg) =>
                    {
                    }, Activator.CreateInstance(item));
                }
            }
        }
        public void OnSystemsReady()
        {
            WarmupBridge();
            Bridge.Request(EndPoint.Server, "OnPlayerReady");
        }
        private void Tick(List<RAGE.Events.TickNametagData> nametags)
        {

        }
    }
}
