using System;

using Newtonsoft.Json;

using Rex.Core.Bridge;

namespace Rex.Client.UI
{
    public class CEFAPI : RexScript
    {
        public CEFAPI()
        {
        }
        public static void SendNetworkMessage(Packet message)
        {
            if (!CEFManager.WindowsReady)
            {
                RAGE.Ui.Console.Log(RAGE.Ui.ConsoleVerbosity.Info, "Попытка отправить сообщения в CEF, браузеры не инциализированы");
                return;
            }
            if (message == null)
            {
                throw new Exception("Message for cef is null");
            }
            else
            {
                var window = CEFManager.GetHtmlWindow(message.Destination);
                window.Call(nameof(Packet), JsonConvert.SerializeObject(message));
            }
        }
    }
}
