using System.Collections.Generic;

using RAGE;
using RAGE.Game;

using Rex.Core.Bridge;
namespace Rex.Client.UI
{
    public class KeyboardState
    {
        public bool CapsLockEnabled { get; set; } = false;
    }
    public class Chat : RexScript
    {
        public static KeyboardState KeyboardState { get; set; } = new KeyboardState();
        public Chat()
        {
            Events.Tick += Tick;
            // RAGE.Input.
        }

        private void Tick(List<Events.TickNametagData> nametags)
        {
            // RAGE.Elements.Player.LocalPlayer.GetSharedData
            if (Pad.IsDisabledControlPressed(0, (int)Control.VehicleMoveDownOnly) && Pad.IsDisabledControlJustPressed(0, (int)Control.VehicleMoveUpOnly))
            {
                // Bridge.Request();
            }
            else if (Pad.IsDisabledControlPressed(0, (int)Control.FrontendSelect))
            {
            }
        }

        // [RexEvent(NetworkPoint.CEFChat)]
        // public GetKeyboardState()
        // {

        // }
        [RexEvent("toggleChat", EndPoint.CEFMain)]
        public void ToggleChat(bool toggle)
        {
            // blah blah
        }
    }
}
