using System;
using Rex.Client.Core;
using Rex.Core.Bridge;
using static RAGE.Elements.Player;
namespace Rex.Client.UI
{
    public class Screenshot : RexScript
    {
        public Screenshot()
        {
            var random = new Random();
            RAGE.Input.Bind(RAGE.Ui.VirtualKeys.F8, true, () =>
            {
                string name = "screenshot" + random.Next(500) + ".png";
                RAGE.Input.TakeScreenshot(name, 1, 100, 100);
                RAGE.Game.Audio.PlaySoundFrontend(-1, "Camera_Shoot", "Phone_Soundset_Franklin", true);
                RAGE.Chat.Output("Making screenshot");
                RAGE.Task.Run(() => Bridge.Request(EndPoint.CEFMain, "takeScreenshot", name), 2000);
            });
            int headshotHandle = 0;
            string headshotTxd = null;
            RAGE.Events.OnPlayerCommand += (cmd, cancel) =>
            {
                if (cmd == "register_headshot")
                {
                    Debugger.DebugLog("Making headshot");
                    headshotHandle = RAGE.Game.Ped.RegisterPedheadshot(LocalPlayer.Handle);
                    RAGE.Task.Run(async () =>
                 {
                     while (!RAGE.Game.Ped.IsPedheadshotReady(headshotHandle))
                     {
                         await RAGE.Task.WaitAsync(200);
                     }
                     headshotTxd = RAGE.Game.Ped.GetPedheadshotTxdString(headshotHandle);
                     Debugger.DebugLog(headshotHandle + " " + headshotTxd);
                     RAGE.Game.Ped.UnregisterPedheadshot(headshotHandle);
                 });
                }
                else if (cmd == "upload_headshot")
                {
                    Debugger.DebugLog("Available?: " + RAGE.Game.Invoker.Invoke<bool>(RAGE.Game.Natives._0xEBB376779A760AA8));

                    Debugger.DebugLog("Requesting upload:");
                    bool requestRes = RAGE.Game.Invoker.Invoke<bool>(RAGE.Game.Natives._0xF0DAEF2F545BEE25, headshotHandle);
                    Debugger.DebugLog(requestRes);

                    Debugger.DebugLog("Releasing");
                    RAGE.Game.Invoker.Invoke(RAGE.Game.Natives._0x5D517B27CF6ECD04, headshotHandle);

                    bool succeded = RAGE.Game.Invoker.Invoke<bool>(RAGE.Game.Natives._0xE8A169E666CBC541);
                    bool failed = RAGE.Game.Invoker.Invoke<bool>(RAGE.Game.Natives._0x876928DDDFCCC9CD);
                    Debugger.DebugLog("Succeded: " + succeded + " Failed: " + failed);
                }
            };
        }
    }
}
