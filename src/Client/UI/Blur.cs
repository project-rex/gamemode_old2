using Rex.Core.Bridge;

namespace Rex.Client.UI
{
    public class Blur : RexScript
    {
        public Blur()
        {
            RAGE.Game.Graphics.TransitionFromBlurred(0);
        }
        [RexEvent("EnableGameBlur", EndPoint.CEFMain)]
        public void EnableGameBlur(bool status)
        {
            if (status)
            {
                RAGE.Game.Graphics.TransitionToBlurred(100);
            }
            else
            {
                RAGE.Game.Graphics.TransitionFromBlurred(100);
            }
        }
    }
}
