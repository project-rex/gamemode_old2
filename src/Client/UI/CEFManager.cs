using System.Collections.Generic;
using RAGE;
using RAGE.Ui;
using Rex.Client.Core;
using Rex.Core.Bridge;

namespace Rex.Client.UI
{
    public class CEFManager : RexScript
    {
        private static readonly Dictionary<EndPoint, HtmlWindow> _windows = new Dictionary<EndPoint, HtmlWindow>();
        public delegate void OnCEFReadyDelegate();
        public static event OnCEFReadyDelegate OnCEFReady;
        public static bool WindowsReady { get; private set; }
        public CEFManager()
        {
            var mainWindow = new HtmlWindow("http://localhost:3000/main.html");
            var devices = new HtmlWindow("http://localhost:3000/devices.html");
            RAGE.Chat.Show(false);
            mainWindow.MarkAsChat();

            _windows.Add(EndPoint.CEFMain, mainWindow);
            _windows.Add(EndPoint.CEFDevices, devices);

            Events.OnBrowserDomReady += OnBrowserDomReady;

            Input.Bind(VirtualKeys.F5, false, () =>
            {
                foreach (var item in _windows)
                {
                    item.Value.Reload(true);
                    item.Value.Reload(false);
                }
            });
        }
        private int _readyBrowsers = 0;
        private void OnBrowserDomReady(HtmlWindow window)
        {
            _readyBrowsers++;
            if (_readyBrowsers == _windows.Count)
            {
                WindowsReady = true;
                Console.Reset();
                Console.Clear();

                Debugger.DebugLog("Все окна cef'а инициализированны");

                OnCEFReady?.Invoke();
            }
        }

        public static HtmlWindow GetHtmlWindow(EndPoint point)
        {
            return _windows[point];
        }
    }
}
