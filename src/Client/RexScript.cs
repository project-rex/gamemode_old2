using System.Globalization;
using Newtonsoft.Json;
using Rex.Client.Core;
using Rex.Core.Bridge;
using Rex.Core.Misc;
namespace Rex.Client
{
    public class RexScript : RAGE.Events.Script
    {
        public RexScript()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            JsonConvert.DefaultSettings = () => JSONSettingsContainer.JSONSettings;
            RAGE.Util.Json.DefaultSettings = () => JSONSettingsContainer.JSONSettings;
            BridgeEventRegistrator.Register(this, Bridge.Instance);
        }
    }
}
