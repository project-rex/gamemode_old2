using System;
using RAGE;
using Rex.Client.Core;

using Rex.Core.Bridge;

namespace Rex.Client.Tests
{
    public class Test : RexScript
    {
        /// <summary>
        /// tbc2
        /// </summary>
        /// <param name="i1"></param>
        [RexEvent(EndPoint.Server)]
        public void TestBridgeServerToClientAction(int i1)
        {
            Chat.Output("TestBridgeServerToClientAction: " + i1);
        }

        // tbc4
        [RexEvent(EndPoint.Server)]
        public int TestBridgeServerToClientHandler(int i1)
        {
            Chat.Output("TestBridgeServerToClientHandler: " + i1);
            return i1;
        }
        // tbc5
        [RexEvent(EndPoint.CEFMain)]
        public int TestCEFBridge()
        {
            return 1337;
        }
        public Test()
        {
            bool stop = false;
            Events.OnPlayerCommand += (string cmd, Events.CancelEventArgs _) =>
            {
                stop = false;
                if (cmd == "tbc1")
                {
                    int t1 = new Random().Next(0, 100);
                    Debugger.DebugLog("[TBC1] Сервер должен вывести в чат: " + t1);
                    Bridge.Request(EndPoint.Server, "TestBridgeClientToServerAction", t1);
                }
                else if (cmd == "tbc3")
                {
                    int t1 = new Random().Next(0, 100);
                    Debugger.DebugLog("[TBC3] Сервер должен вернуть и вывести в консоль: " + t1);

                    Task.Run(async () =>
                 {
                     int result = await Bridge.RequestAsync<int>(EndPoint.Server, "TestBridgeClientToServerHandler", t1);
                     Debugger.DebugLog("Сервер вернул: " + result);
                 });
                }
                else if (cmd == "taskbg")
                {
                    // не работает
                    Debugger.DebugLog("Starting background task");
                    Task.RunBackground(() =>
                    {
                        Task.Run(() =>
                        {
                            Debugger.DebugLog("BG task is executing");
                            Bridge.Request(EndPoint.Server, "TestBridgeClientToServerAction", 228);
                        });
                        // Debugger.DebugLog( "Running in BG: " + RAGE.Game.Misc.GetGameTimer());
                    });
                }
                else if (cmd == "task")
                {
                    // не работает
                    Debugger.DebugLog("Starting task");
                    Task.Run(async () =>
                    {
                        while (!stop)
                        {
                            Debugger.DebugLog("task is executing " + RAGE.Game.Misc.GetGameTimer());
                            await Task.WaitAsync(1000);
                        }
                    });
                }
                else if (cmd == "tbc5")
                {
                    Bridge.Request(EndPoint.CEFMain, "TestAction", 228);
                }
                else if (cmd == "stoptest")
                {
                    stop = true;
                }
            };
        }
    }
}
