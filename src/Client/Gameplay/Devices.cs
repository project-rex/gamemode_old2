using System.Collections.Generic;

using RAGE;
using RAGE.Game;
using Rex.Client.Core;
using Rex.Core.Bridge;

namespace Rex.Client.Gameplay
{
    public class Devices : RexScript
    {
        private static bool s_togglePhone = false;
        public static bool TogglePhone
        {
            get => s_togglePhone;
            set
            {
                Bridge.Request(EndPoint.CEFDevices, "toggleDevice", "phone", value);
                s_togglePhone = value;
            }
        }
        private static bool s_toggleWatch = false;
        public static bool ToggleWatch
        {
            get => s_toggleWatch;
            set
            {
                Bridge.Request(EndPoint.CEFDevices, "toggleDevice", "watch", value);
                s_toggleWatch = value;
            }
        }
        public Devices()
        {
            Events.Tick += Tick;
        }
        [RexEvent("SetPhoneStatus", EndPoint.CEFMain)]
        public void SetPhoneStatus(bool status)
        {
            Devices.TogglePhone = status;
        }
        private void Tick(List<Events.TickNametagData> nametags)
        {
            if (Pad.IsDisabledControlPressed(0, (int)Control.CharacterWheel) && Pad.IsDisabledControlJustPressed(0, (int)Control.FrontendPause))
            {
                if (!TogglePhone)
                {
                    Mobile.CreateMobilePhone(0);
                    Mobile.SetMobilePhoneScale(0);
                    TogglePhone = true;
                }
                else
                {
                    Mobile.DestroyMobilePhone();
                    TogglePhone = false;
                }
            }
        }
    }
}
