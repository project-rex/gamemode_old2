using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using RAGE;
using RAGE.Elements;

namespace Rex.Client.Gameplay
{
    public class Interaction : RexScript
    {
        Ped myPed = new Ped(0x14D7B4E0, new Vector3(-412.13f, 1140.65f, 325.86f), 0f, 0);

        Vector3 vec = new Vector3(0, 0, 0);

        private float DegToRad(float deg)
        {
            return ((float)Math.PI * deg) / 100;
        }

        private Vector3 RotationToDirection(Vector3 rotation)
        {
            var z = DegToRad(rotation.Z);
            var x = DegToRad(rotation.X);
            var num = (float)Math.Abs(Math.Cos(x));

            var result = new Vector3(0, 0, 0);
            result.X = -(float)Math.Sin(z) * num; // warning
            result.Y = (float)Math.Cos(z) * num;
            result.Z = (float)Math.Sin(x);

            return result;
        }

        private class Screen
        {
            public double X { get; set; }
            public double Y { get; set; }
        }
        private Screen ProcessCoordinates(int x, int y)
        {
            int screenX = 0, screenY = 0;
            RAGE.Game.Graphics.GetActiveScreenResolution(ref screenX, ref screenY);

            var relativeX = 1.0 - (Convert.ToDouble(x) / Convert.ToDouble(screenX)) * 1.0 * 2.0;
            var relativeY = 1.0 - (Convert.ToDouble(y) / Convert.ToDouble(screenY)) * 1.0 * 2.0;

            if (relativeX > 0.0)
            {
                relativeX = -relativeX;
            }
            else
            {
                relativeX = Math.Abs(relativeX);
            }

            if (relativeY > 0.0)
            {
                relativeY = -relativeY;
            }
            else
            {
                relativeY = Math.Abs(relativeY);
            }

            return new Screen() { X = relativeX, Y = relativeY };
        }

        public Vector3 WorldToScreen(Vector3 position)
        {
            float x = 0f, y = 0f;

            var pos = RAGE.Game.Graphics.GetScreenCoordFromWorldCoord(position.X, position.Y, position.Z, ref x, ref y);
            if (!pos) return null;

            var newPos = new Vector3(0f, 0f, 0f);
            newPos.X = (x - 0.5f) * 2f;
            newPos.Y = (y - 0.5f) * 2f;
            newPos.Z = 0f;

            return newPos;
        }

        public Vector3 ScreenToWorld(Vector3 camPos, float relX, float relY)
        {
            var eps = 0.001f;
            var camRot = RAGE.Game.Cam._getGameplayCamRot(0);
            var camForward = RotationToDirection(camRot);
            var rotUp = camRot + new Vector3(10f, 0f, 0f);
            var rotDown = camRot + new Vector3(-10f, 0f, 0f);
            var rotLeft = camRot + new Vector3(0f, 0f, -10f);
            var rotRight = camRot + new Vector3(0f, 0f, 10f);
            var camRight = RotationToDirection(rotRight) - RotationToDirection(rotLeft);
            var camUp = RotationToDirection(rotUp) - RotationToDirection(rotDown);
            var rollRad = -DegToRad(camRot.Y);
            var camRightRoll = (camRight * (float)Math.Cos(rollRad)) - (camUp * (float)Math.Sin(rollRad));
            var camUpRoll = camRight * (float)Math.Sin(rollRad) + camUp * (float)Math.Cos(rollRad);
            var point3D = camPos + camForward * 10.0f + camRightRoll + camUpRoll;
            var point2D = WorldToScreen(point3D);
            var point3DZero = camPos + camForward * 10.0f;
            var point2DZero = WorldToScreen(point3DZero);
            var scaleX = (relX - point2DZero.X) / (point2D.X - point2DZero.X);
            var scaleY = (relY - point2DZero.Y) / (point2D.Y - point2DZero.Y);
            var point3Dret = camPos + camForward * 10.0f + camRightRoll * scaleX + camUpRoll * scaleY;
            if (point2D == null)
            {
                return camPos + camForward * 10.0f;
            }
            if (point2DZero == null)
            {
                return camPos + camForward * 10.0f;
            }
            if (
              Math.Abs(point2D.X - point2DZero.X) < eps ||
              Math.Abs(point2D.Y - point2DZero.Y) < eps
            )
            {
                return camPos + camForward * 10.0f;
            }
            return point3Dret;
        }

        public Interaction()
        {
            RAGE.Events.OnClickWithRaycast += OnClick;
            RAGE.Events.Tick += Tick;
        }

        private void OnClick(int x, int y, bool up, bool right, float relativeX, float relativeY, Vector3 _, int __)
        {
            if (up == false) return;

            var camPos = RAGE.Game.Cam.GetGameplayCamCoords();
            var processedCoords = ProcessCoordinates(x, y);
            var target = ScreenToWorld(camPos, (float)processedCoords.X, (float)processedCoords.Y);

            var dir = target - camPos;
            var from = camPos + dir * 0.05f;
            var to = camPos + dir * 300f;

            var rayHandle = RAGE.Game.Shapetest.StartShapeTestRay(
              from.X, from.Y, from.Z, to.X, to.Y, to.Z, 15, 0, 0
            );

            int hit = 0x0;
            Vector3 EndCoords = Vector3.Zero;
            Vector3 SurfaceNormal = Vector3.Zero;
            int materialHash = 0x0;
            int entityHit = 0x0;
            var result = RAGE.Game.Shapetest.GetShapeTestResultEx(rayHandle, ref hit, EndCoords, SurfaceNormal, ref materialHash, ref entityHit);

            _from = from;
            _to = target;

            RAGE.Ui.Console.Log(RAGE.Ui.ConsoleVerbosity.Info, JsonConvert.SerializeObject(EndCoords) + " C# processedCoords");
        }

        Vector3 _from = RAGE.Elements.Player.LocalPlayer.Position;
        Vector3 _to = RAGE.Elements.Player.LocalPlayer.Position;
        private void Tick(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.Graphics.DrawLine(_from.X, _from.Y, _from.Z, _to.X, _to.Y, _to.Z, 255, 0, 0, 255);
        }
    }
}