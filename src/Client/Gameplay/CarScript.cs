using System;
using System.Collections.Generic;
using RAGE;
using RAGE.Elements;
using Rex.Client.Core;
using Rex.Client.Core.Data;
using Rex.Core.Bridge;
using Rex.Domain.Models.Vehicles;

namespace Rex.Client.Gameplay
{
    public class CarScript : RexScript
    {
        void OnIndicatorsChanged(Vehicle car, LinkedList<object> targetTree, IndicatorsState indicators, IndicatorsState oldValue)
        {
            if (indicators == IndicatorsState.All)
            {
                car.SetIndicatorLights((int)IndicatorLight.Left, true);
                car.SetIndicatorLights((int)IndicatorLight.Right, true);
            }
            else
            {
                car.SetIndicatorLights((int)IndicatorLight.Left, indicators == IndicatorsState.Left);
                car.SetIndicatorLights((int)IndicatorLight.Right, indicators == IndicatorsState.Right);
            }
        }
        void OnWindowRolledDownChanged(Vehicle veh, LinkedList<object> targetTree, bool rolledDown, bool oldValue)
        {
            var window = targetTree.Last.Previous.Value as Window;
            var car = targetTree.First.Value as Car;
            int windowIndex = car.Windows.IndexOf(window);
            if (rolledDown)
            {
                veh.RollDownWindow(windowIndex);
            }
            else
            {
                veh.RollUpWindow(windowIndex);
            }
        }
        public CarScript()
        {
            DataSync.AddHandler<Vehicle, Car, IndicatorsState>((car) => car.Indicators, OnIndicatorsChanged);
            DataSync.AddHandler<Vehicle, Car, bool>((car) => car.Windows[0].RolledDown, OnWindowRolledDownChanged);
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad1, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                if (state.Indicators == IndicatorsState.Left)
                {
                    state.Indicators = IndicatorsState.Disabled;
                    return;
                }
                state.Indicators = IndicatorsState.Left;
                Debugger.DebugLog("Indicators state changed");

            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad3, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                if (state.Indicators == IndicatorsState.Right)
                {
                    state.Indicators = IndicatorsState.Disabled;
                    return;
                }
                state.Indicators = IndicatorsState.Right;
            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad2, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                if (state.Indicators == IndicatorsState.All)
                {
                    state.Indicators = IndicatorsState.Disabled;
                    return;
                }
                state.Indicators = IndicatorsState.All;
            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad7, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                state.Windows[0].RolledDown = !state.Windows[0].RolledDown;
            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad9, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                state.Windows[1].RolledDown = !state.Windows[1].RolledDown;
            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad4, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                state.Windows[2].RolledDown = !state.Windows[2].RolledDown;

            });
            Input.Bind(RAGE.Ui.VirtualKeys.Numpad6, true, () =>
            {
                if (Player.LocalPlayer.Vehicle == null) return;
                var state = Player.LocalPlayer.Vehicle.GetState() as Car;
                state.Windows[3].RolledDown = !state.Windows[3].RolledDown;
            });

            Events.OnPlayerCommand += (string cmd, Events.CancelEventArgs args) =>
            {
                string[] ps = cmd.Trim().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                Debugger.DebugLog(cmd + " " + args.ToString());
                if (ps[0] == "setwheeltype")
                {
                    if (Player.LocalPlayer.Vehicle != null)
                    {
                        int type = int.Parse(ps[1]);
                        Player.LocalPlayer.Vehicle.SetWheelType(type);
                        Chat.Output("Колеса установлены");
                    }
                    else
                    {
                        Chat.Output("Вы не в транспорте");
                    }
                    args.Cancel = true;
                }
                else if (ps[0] == "setvehmod")
                {
                    if (Player.LocalPlayer.Vehicle != null && ps.Length == 3)
                    {
                        int type = int.Parse(ps[1]);
                        int mod = int.Parse(ps[2]);
                        Player.LocalPlayer.Vehicle.SetMod(type, mod, false);
                        Chat.Output("Мод установлены");
                    }
                    else
                    {
                        Chat.Output("неверная команда");
                    }
                    args.Cancel = true;
                }
                else if (ps[0] == "test_ser")
                {
                    RAGE.Events.CallRemote("any", new Packet(EndPoint.Server, EndPoint.Client, "adasdas", null));
                    args.Cancel = true;
                }
            };
        }
        public enum IndicatorLight
        {
            Right = 0,
            Left = 1
        }
    }
}
