using RAGE.Ui;

using Rex.Core.Misc;
namespace Rex.Client.Core
{
    public sealed class Debugger : IDebugger
    {
        private static Debugger debugger;
        public static IDebugger Instance
        {
            get
            {
                if (debugger == null)
                {
                    debugger = new Debugger();
                }
                return debugger;
            }
        }

        public void DebugLog(string message)
        {
            Console.LogLine(ConsoleVerbosity.Info, $"[{System.DateTime.Now.ToShortTimeString()}] [CLIENT] {message}", false, false);
        }
        public void DebugWarn(string message)
        {
            Console.LogLine(ConsoleVerbosity.Warning, $"[{System.DateTime.Now.ToShortTimeString()}] [CLIENT] {message}", false, false);
        }
        public void DebugError(string message)
        {
            Console.LogLine(ConsoleVerbosity.Error, $"[{System.DateTime.Now.ToShortTimeString()}] [CLIENT] {message}", false, false);
        }

        public static void DebugLog(object message)
        {
            Instance.DebugLog(message?.ToString());
        }
        public static void DebugWarn(object message)
        {
            Instance.DebugWarn(message?.ToString());
        }
        public static void DebugError(object message)
        {
            Instance.DebugError(message?.ToString());
        }
    }
}
