using System;
using System.Threading.Tasks;
using Rex.Client.UI;
using Rex.Core.Bridge;
using Rex.Core.Misc;

namespace Rex.Client.Core
{
    public class Bridge : BridgeBase
    {
        private static Bridge s_instance;
        public static Bridge Instance
        {
            get
            {
                return s_instance ?? (s_instance = new Bridge());
            }
        }
        public override EndPoint CurrentEndPoint { get; } = EndPoint.Client;
        protected override ICallbackInvoker CBInvoker { get; } = new ClientCallbackInvoker();
        protected override IDebugger Logger { get; } = new Debugger();

        public static void Request(EndPoint point, string fName, params object[] data)
        {
            s_instance.InternalRequest(null, point, fName, data);
        }
        public static void Request(EndPoint point, string fName, Action<object> action, params object[] data)
        {
            s_instance.InternalRequest(null, point, fName, action, data);
        }
        public static Task<T> RequestAsync<T>(EndPoint point, string fName, params object[] data)
        {
            return s_instance.InternalRequestAsync<T>(null, point, fName, data);
        }

        protected override void SendMessage(Packet message, object receiver)
        {
            switch (message.Destination)
            {
                case EndPoint.Server:
                    RAGE.Events.CallRemote(nameof(Packet), message);
                    break;
                case EndPoint.Client:
                    throw new ArgumentException("Сообщение не может быть отправлено самому себе!");
                case EndPoint.CEFMain:
                case EndPoint.CEFDevices:
                    CEFAPI.SendNetworkMessage(message);
                    break;
            }
        }
    }
}
