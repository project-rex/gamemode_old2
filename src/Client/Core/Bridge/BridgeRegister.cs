using System;

using Newtonsoft.Json;

using Rex.Core.Bridge;
namespace Rex.Client.Core
{
    public class BridgeRegister : RexScript
    {
        public BridgeRegister()
        {
            RAGE.Events.Add(nameof(Packet), (args) =>
            {
                object d = args[0];
                if (d is Packet message)
                {
                    Bridge.Instance.OnMessageReceived(message, null);
                }
                else if (d is string str)
                {
                    message = JsonConvert.DeserializeObject<Packet>(str);
                    Bridge.Instance.OnMessageReceived(message, null);
                }
                else
                {
                    throw new Exception("Exception in casting object to Network Message. " + args[0].GetType() + " " + JsonConvert.SerializeObject(args[0]));
                }
            });
        }
    }
}
