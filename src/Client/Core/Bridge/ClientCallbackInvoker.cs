using System;
using System.Threading.Tasks;

using Rex.Core.Bridge;
using Rex.Core.Misc;
namespace Rex.Client.Core
{
    public class ClientCallbackInvoker : ICallbackInvoker
    {
        public void InvokeAction(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parameters = msgData;
            if (eventInfo.ParseParameters)
            {
                parameters = BridgeArgumentParser.ConvertArgsToParameters(msgData, eventInfo.Callback.GetParameters());
            }
            if (eventInfo.Optimize)
            {
                var func = FastExpressionInvoker.GetActionExpression(eventInfo.Callback);
                func.Invoke(eventInfo.Target, parameters);
            }
            else
            {
                eventInfo.Callback.Invoke(eventInfo.Target, parameters);
            }
        }

        public object InvokeHandler(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parameters = msgData;
            if (eventInfo.ParseParameters)
            {
                parameters = BridgeArgumentParser.ConvertArgsToParameters(msgData, eventInfo.Callback.GetParameters());
            }
            if (eventInfo.Optimize)
            {
                var func = FastExpressionInvoker.GetFuncExpression(eventInfo.Callback);
                return func.Invoke(eventInfo.Target, parameters);
            }
            else
            {
                return eventInfo.Callback.Invoke(eventInfo.Target, parameters);
            }
        }

        [Obsolete("Может привести к крашу на клиенте")]
        public Task<object> InvokeHandlerAsync(BridgeEventInfo eventInfo, object[] msgData, object sender)
        {
            object[] parsedParams = BridgeArgumentParser.ConvertArgsToParameters(msgData, eventInfo.Callback.GetParameters());
            if (eventInfo.Optimize)
            {
                var funcExpr = FastExpressionInvoker.GetFuncExpression(eventInfo.Callback);
                return (Task<object>)funcExpr.Invoke(eventInfo.Target, parsedParams);
            }
            else
            {
                return (Task<object>)eventInfo.Callback.Invoke(eventInfo.Target, parsedParams);
            }
        }
    }
}
