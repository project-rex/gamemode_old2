using Rex.Domain.Models.Characters;
using Rex.Domain.Models.Vehicles;

namespace Rex.Client.Core.Data
{
    public static class StateExtensions
    {
        public static VehicleBase GetState(this RAGE.Elements.Vehicle vehicle) => vehicle.GetData<VehicleBase>("State");
        public static Character GetState(this RAGE.Elements.Player player) => player.GetData<Character>("State");
    }
}
