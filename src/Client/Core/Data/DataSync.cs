using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using RAGE;
using RAGE.Elements;
using Rex.Core.Bridge;
using Rex.Core.Data;

namespace Rex.Client.Core.Data
{
    public class DataSync : RexScript
    {
        private static readonly Dictionary<Entity, EntityChangedEventHandler> outputHandlers = new Dictionary<Entity, EntityChangedEventHandler>();
        private static readonly Dictionary<string, OnEntityPropertyDataChangedDelegate> inputHandlers = new Dictionary<string, OnEntityPropertyDataChangedDelegate>();

        private readonly DataController controller;
        public DataSync()
        {
            controller = new DataController(Debugger.Instance);
            Events.OnEntityStreamIn += OnEntityStreamIn;
            Events.OnEntityStreamOut += OnEntityStreamOut;
        }

        public delegate void OnEntityPropertyDataChangedDelegate<TEntity, TProperty>(TEntity entity, LinkedList<object> targetsTree, TProperty newValue, TProperty oldValue)
            where TEntity : Entity;

        public delegate void OnEntityPropertyDataChangedDelegate(object entity, LinkedList<object> targetsTree, object newValue, object oldValue);

        public static void AddHandler<TEntity, TEntityData, TProperty>(Expression<Func<TEntityData, object>> expression, OnEntityPropertyDataChangedDelegate<TEntity, TProperty> action)
            where TEntityData : NotifyPropertyChangedBase
            where TEntity : Entity
        {
            string propertyPath = NameOf.Full(expression);
            inputHandlers.Add(DataController.MakePathGeneric(propertyPath), (entity, targetTree, newValue, oldValue) => action.Invoke((TEntity)entity, targetTree, (TProperty)newValue, (TProperty)oldValue));
        }
        [RexEvent(EndPoint.Server)]
        public void ApplySyncData(int eType, int remoteId, EntityChangedEventArgs args)
        {
            var entityType = (RAGE.Elements.Type)eType;
            Entity entity;
            switch (entityType)
            {
                case RAGE.Elements.Type.Player:
                    entity = Entities.Players.GetAtRemote((ushort)remoteId);
                    break;
                case RAGE.Elements.Type.Vehicle:
                    entity = Entities.Vehicles.GetAtRemote((ushort)remoteId);
                    break;
                default:
                    throw new NotImplementedException();
            }
            var state = entity.GetData<NotifyPropertyChangedBase>("State");
            var commitInfo = controller.CommitChange(state, args);
            string genericPath = DataController.MakePathGeneric(commitInfo.PublicPath);
            inputHandlers[genericPath].Invoke(entity, commitInfo.TargetTree, commitInfo.NewValue, commitInfo.OldValue);
        }

        private EntityChangedEventHandler CreateEntityHandler(Entity e)
        {
            return (sender, args) => SyncStatePropertyChange(e, args);
        }
        public void SyncStatePropertyChange(Entity entity, EntityChangedEventArgs args)
        {
            Events.CallRemote("CommitEntityData", entity.Type, entity.RemoteId, args);
        }
        private void OnEntityStreamIn(Entity entity)
        {
            Bridge.Request(EndPoint.Server, "GetEntitySyncData", (arg) =>
            {
                var state = (NotifyPropertyChangedBase)arg;
                var tracker = ChangeTracker.Create(state);
                var trackerHandler = CreateEntityHandler(entity);
                tracker.EntityChanged += trackerHandler;
                outputHandlers.Add(entity, trackerHandler);
                entity.SetData("State", state);
            }, entity.Type, entity.RemoteId);

        }
        private void OnEntityStreamOut(Entity entity)
        {
            var state = entity.GetData<NotifyPropertyChangedBase>("State");
            if (outputHandlers.ContainsKey(entity))
            {
                var tracker = outputHandlers[entity];
                state.EntityChanged -= tracker;
                outputHandlers.Remove(entity);
                entity.SetData<NotifyPropertyChangedBase>("State", null);
            }
        }
    }
}
