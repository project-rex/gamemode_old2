namespace Rex.Domain
{
    public static class ChatColor
    {
        public static string Red(this string s)
        {
            return Col("r") + s;
        }

        public static string Blue(this string s)
        {
            return Col("b") + s;
        }

        public static string Green(this string s)
        {
            return Col("g") + s;
        }

        public static string Yellow(this string s)
        {
            return Col("y") + s;
        }

        public static string Purple(this string s)
        {
            return Col("p") + s;
        }

        public static string Pink(this string s)
        {
            return Col("q") + s;
        }

        public static string Orange(this string s)
        {
            return Col("o") + s;
        }

        public static string Grey(this string s)
        {
            return Col("c") + s;
        }

        public static string GreyDark(this string s)
        {
            return Col("m") + s;
        }

        public static string Black(this string s)
        {
            return Col("u") + s;
        }

        public static string NewLine(this string s)
        {
            return Col("n") + s;
        }

        public static string WhiteDefault(this string s)
        {
            return Col("s") + s;
        }

        public static string White(this string s)
        {
            return Col("w") + s;
        }

        public static string Bold(this string s)
        {
            return Col("h") + s;
        }

        private static string Col(string c)
        {
            return "~" + c + "~";
        }

        public static string WWhite(this string s)
        {
            return WCol(s, "white");
        }

        public static string WRed(this string s)
        {
            return WCol(s, "red");
        }

        public static string WBlue(this string s)
        {
            return WCol(s, "blue");
        }

        public static string WGreen(this string s)
        {
            return WCol(s, "green");
        }

        public static string WYellow(this string s)
        {
            return WCol(s, "yellow");
        }

        public static string WOrange(this string s)
        {
            return WCol(s, "orange");
        }

        public static string WPink(this string s)
        {
            return WCol(s, "pink");
        }

        public static string WCol(string text, string color)
        {
            string[] words = text.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = "!{" + color + "}" + words[i];
            }
            return string.Join(" ", words);
        }
    }
}
