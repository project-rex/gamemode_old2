namespace Rex.Domain.Settings
{
    public class GlobalSettings
    {
        public float TimeSpeedMultiplier { get; set; }
    }
}
