namespace Rex.Domain.Models
{
    public class TestClass
    {
        public string TestString { get; set; }
        public float TestFloat { get; set; }
        public byte TestByte { get; set; }
    }
}
