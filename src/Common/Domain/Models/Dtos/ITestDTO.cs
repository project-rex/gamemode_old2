using System.Collections.Generic;

namespace Rex.Domain.Models.Dtos
{
    interface ITestDTO
    {
        string RequestSomething(int n1, string s2, List<int> l3);
    }
}
