using System;

namespace Rex.Domain.Models.Game
{
    public class Vector3
    {
        public Guid Id { get; set; }

        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}
