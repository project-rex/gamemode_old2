using System;

namespace Rex.Domain.Models.Penalties
{
    public abstract class PenaltyBase
    {
        public Guid Id { get; set; }
        public DateTime ReceivingDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Cause { get; set; }

        protected PenaltyBase(DateTime receivingDate, DateTime endDate, string cause)
        {
            ReceivingDate = receivingDate;
            EndDate = endDate;
            Cause = cause;
        }
    }
}
