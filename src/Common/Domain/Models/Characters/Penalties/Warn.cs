using System;

namespace Rex.Domain.Models.Penalties
{
    public class Warn : PenaltyBase
    {
        public Warn(DateTime receivingDate, DateTime endDate, string cause) : base(receivingDate, endDate, cause)
        {
        }

        public string Reason { get; set; }
        public DateTime Issued { get; set; }
        public string AdminName { get; set; }
        /// <summary>
        /// Ид аккаунта админа
        /// </summary>
        public Guid AdminId { get; set; }
    }
}
