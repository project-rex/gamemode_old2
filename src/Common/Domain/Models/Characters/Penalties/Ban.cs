using System;

namespace Rex.Domain.Models.Penalties
{
    public class Ban : PenaltyBase
    {
        public Ban(DateTime receivingDate, DateTime endDate, string cause) : base(receivingDate, endDate, cause)
        {
        }

        public string Reason { get; set; }
    }
}
