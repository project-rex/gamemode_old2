using System;

using Rex.Domain.Models.Game;

namespace Rex.Domain.Models.Penalties
{
    public class Jail : PenaltyBase
    {
        public Jail(DateTime receivingDate, DateTime endDate, string cause) : base(receivingDate, endDate, cause)
        {
        }

        public bool IsInJail { get; set; }
        public Vector3 Position { get; set; }
        public ulong SecondsToRelease { get; set; }
        public bool IsDemorgan { get; set; }
    }
}
