using System;

namespace Rex.Domain.Models.Penalties
{
    public class Mute : PenaltyBase
    {
        public Mute(DateTime receivingDate, DateTime endDate, string cause) : base(receivingDate, endDate, cause)
        {
        }

        public bool IsMuted { get; set; }
        public ulong SecondsToRelease { get; set; }
    }
}
