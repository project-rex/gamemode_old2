namespace Rex.Domain.Models.Customization
{
    public class Heredity
    {
        public int Father { get; set; }
        public int Mother { get; set; }
        public int Similarity { get; set; }
        public int SkinSimilarity { get; set; }
    }
}
