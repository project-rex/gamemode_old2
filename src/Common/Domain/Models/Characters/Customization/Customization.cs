namespace Rex.Domain.Models.Customization
{
    public class Customization
    {
        public float[] FaceFeatures { get; set; }
        public float[] Appearance { get; set; }
        public Head Head { get; set; }
        public Heredity Heredity { get; set; }
    }
}
