namespace Rex.Domain.Models.Customization
{
    public class Head
    {
        public int Hair { get; set; }
        public int HairColor { get; set; }
        public int HairHighlight { get; set; }
        public int EyebrowColor { get; set; }
        public int BeardColor { get; set; }
        public int BlushColor { get; set; }
        public int LipstickColor { get; set; }
        public int ChestHairColor { get; set; }
        public int EyeColor { get; set; }
    }
}
