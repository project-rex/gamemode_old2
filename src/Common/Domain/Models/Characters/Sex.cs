namespace Rex.Domain.Models.Characters
{
    public enum Sex
    {
        Male,
        Female
    }
}
