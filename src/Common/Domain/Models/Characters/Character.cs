using System;
using Rex.Core.Data;
using Rex.Domain.Models.Game;

namespace Rex.Domain.Models.Characters
{
    public class Character : NotifyPropertyChangedBase
    {
        public Guid Id { get; set; }
        public int StaticId { get; set; }
        public string Name { get; set; }

        public Vector3 Position { get; set; }
        public float Heading { get; set; }
        public Sex Model { get; set; }
        public DateTime DateOfBirth { get; set; }

        public DateTime LastSeen { get; set; }
        [IgnoreSync]
        public decimal Cash { get; set; }

        public float Hunger { get; set; }
        public float Thirst { get; set; }
        public float Strength { get; set; }
        private float _endurance;

        public Character()
        {
        }

        public float Endurance
        {
            get => _endurance;
            set => Set(ref _endurance, value);
        }
        public SyncableCollection<Character> Friends { get; set; } = new SyncableCollection<Character>();
    }
}
