namespace Rex.Domain.Models.Characters
{
    public class Status
    {
        public int Radiation { get; set; }
        public int Stamina { get; set; }
        public int Death { get; set; }
        public int Vest { get; set; }
    }
}
