namespace Rex.Domain.Models
{
    public enum Licenses
    {
        Bike,
        Car,
        Bus,
        Truck,
        Air
    }
}
