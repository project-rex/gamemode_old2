using System;

namespace Rex.Domain.Models.Vehicles
{
    [Flags]
    public enum IndicatorsState
    {
        Disabled,
        Left,
        Right,
        All
    }
}
