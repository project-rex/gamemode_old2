using Rex.Core.Data;
namespace Rex.Domain.Models.Vehicles
{
    public class Car : VehicleBase
    {
        public string NumberPlate { get; set; }

        private IndicatorsState indicators;

        public Car()
        {
        }

        public IndicatorsState Indicators
        {
            get => indicators;
            set => Set(ref indicators, value);
        }
        public SyncableCollection<Wheel> Wheels { get; set; } = new SyncableCollection<Wheel>();

        public SyncableCollection<Window> Windows { get; set; } = new SyncableCollection<Window>();
    }
    public class Wheel : NotifyPropertyChangedBase
    {
        public int DiskModel { get; set; }

        private bool tyreBurst;

        public Wheel()
        {
        }

        public bool TyreBurst
        {
            get => tyreBurst;
            set => Set(ref tyreBurst, value);
        }
    }
    public class Window : NotifyPropertyChangedBase
    {
        private bool rolledDown;
        public bool RolledDown { get => rolledDown; set => Set(ref rolledDown, value); }

        private int tint;
        public int Tint { get => tint; set => Set(ref tint, value); }

        public int WindowIndex { get; set; }

        public Window(int windowIndex) => WindowIndex = windowIndex;

        public Window()
        {
        }
    }
}
