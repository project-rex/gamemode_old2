using System;

namespace Rex.Domain.Models
{
    [Flags]
    public enum Role
    {
        Player,
        Moderator,
        Administrator,
        Super
    }
}
