using System;

namespace Rex.Domain.Models
{
    public class Item
    {
        public Guid Id { get; set; }
        public string Name { get; set; } // Идентификатор предмета
        public ulong Amount { get; set; }
        public string Description { get; set; }
    }
}
