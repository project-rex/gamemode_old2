using System;

namespace Rex.Domain.Models
{
    public class Session
    {
        public Guid Id { get; set; }
        public DateTime Time { get; set; } // время авторизации
        public string IP { get; set; } // Адрес
        public string Serial { get; set; } // Железо
    }
}
