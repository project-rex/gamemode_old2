namespace Rex.Domain.Models
{
    public enum AccountPermissions
    {
        Player,
        Helper,
        Moderator,
        Admin,
        MainAdmin,
        Master,
        Root,
    }
}
