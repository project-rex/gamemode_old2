using System.Collections.Generic;
using Rex.Core.Data;
using Rex.Domain.Models.Characters;
using Rex.Domain.Models.Penalties;
namespace Rex.Domain.Models
{
    public class User : NotifyPropertyChangedBase
    {
        public ulong ScId { get; set; } // ID Social Club
        public string Email { get; set; }
        public decimal DonateBalance { get; set; }
        public string PasswordHash { get; set; }
        public AccountPermissions Permissions { get; set; }
        public List<Ban> Bans { get; set; }
        public List<Warn> Warns { get; set; }

        public SyncableCollection<Character> Characters { get; set; } = new SyncableCollection<Character>();
        public List<Session> LastSessions { get; set; } // Последние сессии игрока с лимитом в ~30 штук
        public Session RegisterSession { get; set; } // Первая сессия игрока
    }
}
