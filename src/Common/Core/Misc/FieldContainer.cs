using System.Reflection;

namespace Rex.Core.Data
{
    public class FieldContainer
    {
        public FieldContainer(FieldInfo fieldInfo, object lastTarget)
        {
            FieldInfo = fieldInfo;
            LastTarget = lastTarget;
        }

        public FieldInfo FieldInfo { get; set; }
        public object LastTarget { get; set; }
    }
}
