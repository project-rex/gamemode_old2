namespace Rex.Core.Misc
{
    public interface IDebugger
    {
        void DebugLog(string message);
        void DebugWarn(string message);
        void DebugError(string message);
    }
}
