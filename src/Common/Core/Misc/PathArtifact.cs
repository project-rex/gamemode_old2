namespace Rex.Core.Misc
{
    public static partial class Helpers
    {
        public class PathArtifact
        {
            public PathArtifact(string member)
            {
                Key = member;
            }
            public PathArtifact(int index)
            {
                IsIndex = true;
                Index = index;
            }

            public string Key { get; set; }
            public bool IsIndex { get; set; }
            public int Index { get; set; }
        }
    }
}
