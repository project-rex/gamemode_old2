﻿using System;
using System.Linq;
using Newtonsoft.Json.Serialization;
namespace Rex.Core.Misc
{
    public class KnownTypesBinder : ISerializationBinder
    {
        public Type BindToType(string assemblyName, string typeName)
        {
            return DomainTypesContainer.DomainTypes.First(x => x.Name == typeName);
        }
        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }
    }
}
