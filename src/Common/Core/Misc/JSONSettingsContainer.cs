using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Rex.Core.Misc
{
    public static class JSONSettingsContainer
    {
        public static KnownTypesBinder knownTypesBinder { get; } = new KnownTypesBinder();
        public static JsonSerializerSettings JSONSettings { get; } = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects,
            SerializationBinder = knownTypesBinder,
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
            Formatting = Formatting.None,
            // Converters = new List<JsonConverter>() {
            //     new Newtonsoft.Json.Converters.StringEnumConverter(new CamelCaseNamingStrategy(), true),
            //     // new Newtonsoft.Json.Converters.
            // },
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            StringEscapeHandling = StringEscapeHandling.Default,
            DefaultValueHandling = DefaultValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };
    }
}
