using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Rex.Core.Misc
{
    public static class FastExpressionInvoker
    {
        private static readonly Dictionary<MethodInfo, Func<object, object[], object>> funcExpressionsByMethodInfoCache = new Dictionary<MethodInfo, Func<object, object[], object>>();
        private static readonly Dictionary<MethodInfo, Action<object, object[]>> actionExpressionsByMethodInfoCache = new Dictionary<MethodInfo, Action<object, object[]>>();

        public static void CacheMethod(MethodInfo methodInfo)
        {
            if (methodInfo.ReturnType == typeof(void))
            {
                if(actionExpressionsByMethodInfoCache.ContainsKey(methodInfo)) return;
                actionExpressionsByMethodInfoCache.Add(methodInfo, MethodInfoToFastActionLambdaInvocation(methodInfo));
            }
            else
            {
                 if(funcExpressionsByMethodInfoCache.ContainsKey(methodInfo)) return;
                funcExpressionsByMethodInfoCache.Add(methodInfo, MethodInfoToFastFuncLambdaInvocation(methodInfo));
            }
        }
        public static Func<object, object[], object> GetFuncExpression(MethodInfo methodInfo)
        {
            return funcExpressionsByMethodInfoCache[methodInfo];
        }
        public static Action<object, object[]> GetActionExpression(MethodInfo methodInfo)
        {
            return actionExpressionsByMethodInfoCache[methodInfo];
        }
        public static Func<object, object[], object> MethodInfoToFastFuncLambdaInvocation(
            MethodInfo pMethodInfo
         )
        {
            var instanceParameterExpression = Expression.Parameter(typeof(object));
            var argumentsParameterExpression = Expression.Parameter(typeof(object[]));

            int index = 0;
            var argumentExtractionExpressions =
               pMethodInfo
               .GetParameters()
               .Select(parameter =>
                  Expression.Convert(
                     Expression.ArrayAccess(
                        argumentsParameterExpression,
                        Expression.Constant(index++)
                     ),
                     parameter.ParameterType
                  )
               ).ToList();

            var callExpression = pMethodInfo.IsStatic
               ? Expression.Call(pMethodInfo, argumentExtractionExpressions)
               : Expression.Call(
                  Expression.Convert(
                     instanceParameterExpression,
                     pMethodInfo.DeclaringType
                  ),
                  pMethodInfo,
                  argumentExtractionExpressions
               );

            var endLabel = Expression.Label(typeof(object));
            var finalExpression = Expression.Convert(callExpression, typeof(object));

            var lambdaExpression = Expression.Lambda<Func<object, object[], object>>(
               finalExpression,
               instanceParameterExpression,
               argumentsParameterExpression
            );
            var compiledLambda = lambdaExpression.Compile();

            return compiledLambda;
        }
        public static Action<object, object[]> MethodInfoToFastActionLambdaInvocation(
            MethodInfo pMethodInfo
         )
        {
            var instanceParameterExpression = Expression.Parameter(typeof(object));
            var argumentsParameterExpression = Expression.Parameter(typeof(object[]));

            int index = 0;
            var argumentExtractionExpressions =
               pMethodInfo
               .GetParameters()
               .Select(parameter =>
                  Expression.Convert(
                     Expression.ArrayAccess(
                        argumentsParameterExpression,
                        Expression.Constant(index++)
                     ),
                     parameter.ParameterType
                  )
               ).ToList();

            var callExpression = pMethodInfo.IsStatic
               ? Expression.Call(pMethodInfo, argumentExtractionExpressions)
               : Expression.Call(
                  Expression.Convert(
                     instanceParameterExpression,
                     pMethodInfo.DeclaringType
                  ),
                  pMethodInfo,
                  argumentExtractionExpressions
               );

            var endLabel = Expression.Label(typeof(object));
            var finalExpression = (Expression)Expression.Block(
                    callExpression,
                    Expression.Return(endLabel, Expression.Constant(null)),
                    Expression.Label(endLabel, Expression.Constant(null))
                 );
            var lambdaExpression = Expression.Lambda<Action<object, object[]>>(
               finalExpression,
               instanceParameterExpression,
               argumentsParameterExpression
            );
            return lambdaExpression.Compile();
        }
    }
}
