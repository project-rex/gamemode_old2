using System.Reflection;

namespace Rex.Core.Data
{
    public class PropertyContainer
    {
        public PropertyContainer(PropertyInfo propertyInfo, object lastTarget)
        {
            PropertyInfo = propertyInfo;
            LastTarget = lastTarget;
        }

        public PropertyInfo PropertyInfo { get; set; }
        public object LastTarget { get; set; }
    }
}
