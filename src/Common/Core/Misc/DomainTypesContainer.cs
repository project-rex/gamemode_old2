//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Rex.Core
{


    public class DomainTypesContainer
    {

        public static readonly HashSet<Type> DomainTypes = new HashSet<Type> {
                typeof(Rex.Domain.Settings.ChatSettings),
                typeof(Rex.Domain.Settings.GlobalSettings),
                typeof(Rex.Domain.Settings.Local.KeyboardSettings),
                typeof(Rex.Domain.Models.TestClass),
                typeof(Rex.Domain.Models.Item),
                typeof(Rex.Domain.Models.Session),
                typeof(Rex.Domain.Models.User),
                typeof(Rex.Domain.Models.Vehicles.Car),
                typeof(Rex.Domain.Models.Vehicles.Window),
                typeof(Rex.Domain.Models.Vehicles.Wheel),
                typeof(Rex.Domain.Models.Vehicles.VehicleBase),
                typeof(Rex.Domain.Models.Game.Vector3),
                typeof(Rex.Domain.Models.Dtos.UpdateIndicatorsDto),
                typeof(Rex.Domain.Models.Penalties.Ban),
                typeof(Rex.Domain.Models.Penalties.Jail),
                typeof(Rex.Domain.Models.Penalties.Mute),
                typeof(Rex.Domain.Models.Penalties.Warn),
                typeof(Rex.Domain.Models.Customization.Customization),
                typeof(Rex.Domain.Models.Customization.Heredity),
                typeof(Rex.Domain.Models.Customization.Head),
                typeof(Rex.Domain.Models.Characters.Character),
                typeof(Rex.Domain.Models.Characters.Status),
                typeof(Rex.Core.Misc.KnownTypesBinder),
                typeof(Rex.Core.Bridge.BridgeEventInfo),
                typeof(Rex.Core.Bridge.BridgeEventRegistrator),
                typeof(Rex.Core.Data.EntityChangedEventArgs),
                typeof(Rex.Core.Data.NotifyPropertyChangedEventArgs),
                typeof(_System.Collections.Specialized.NotifyCollectionChangedEventArgs),
                typeof(Rex.Core.Bridge.Packet),
                typeof(System.Collections.Generic.List<>),
                typeof(System.Collections.Generic.List<object>),
                typeof(System.Collections.Generic.Dictionary<, >),
                typeof(System.Collections.Generic.Dictionary<object, object>),
                typeof(System.Collections.Generic.HashSet<object>),
                typeof(System.Collections.Generic.LinkedList<object>)};
    }
}
