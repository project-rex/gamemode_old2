using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Rex.Core.Misc
{
    public static partial class Helpers
    {
        public static IEnumerable<MethodInfo> GetTypeMethodsWithAttribute(Type objectType, Type attributeType)
        {
            return objectType
               .GetMethods()
               .Where(m => m.GetCustomAttributes(attributeType, true).Length > 0);
        }
        public static int LevenshteinDistance(string string1, string string2)
        {
            if (string1 == null)
            {
                throw new ArgumentNullException(nameof(string1));
            }

            if (string2 == null)
            {
                throw new ArgumentNullException(nameof(string2));
            }

            int diff;
            int[,] m = new int[string1.Length + 1, string2.Length + 1];

            for (int i = 0; i <= string1.Length; i++)
            {
                m[i, 0] = i;
            }
            for (int j = 0; j <= string2.Length; j++)
            {
                m[0, j] = j;
            }

            for (int i = 1; i <= string1.Length; i++)
            {
                for (int j = 1; j <= string2.Length; j++)
                {
                    diff = (string1[i - 1] == string2[j - 1]) ? 0 : 1;

                    m[i, j] = Math.Min(Math.Min(m[i - 1, j] + 1,
                            m[i, j - 1] + 1),
                        m[i - 1, j - 1] + diff);
                }
            }
            return m[string1.Length, string2.Length];
        }
        public static List<string> FindSimiliarNamesInEnum(Type enumType, string name, int sensivity = 5, bool lower = true)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Ожидался Enum, был получен " + enumType.Name);
            }

            if (lower)
            {
                name = name.ToLower();
            }

            var similiarNames = new List<string>();

            if (name.Length > 1)
            {
                similiarNames.AddRange(Enum.GetNames(enumType)
                    .Select(x => x.ToLower())
                    .Where(x => LevenshteinDistance(name, x) < sensivity || (name.Length > 3 && name.StartsWith(x)))
                    .ToList());
            }
            return similiarNames;
        }
        // public static PropertyInfo GetStaticProperty(Type type, string path, BindingFlags flags = DefaultFlags | BindingFlags.Static)
        // {
        //     return GetProperty(type,);
        // }
        public static (T, LinkedList<object>) Get<T>(Func<Type, string, BindingFlags, T> keyGetter,
                                         Func<T, object, object> valueGetter,
                                         Func<T, Type> typeGetter,
                                         object obj,
                                         string path,
                                         BindingFlags flags)
        where T : MemberInfo
        {
            var tree = new LinkedList<object>();
            tree.AddFirst(tree);
            string[] paths = path.Split(".");
            if (path.Length == 0)
            {
                throw new ArgumentException("Неправильный путь до поля");
            }
            if (obj == null)
            {
                throw new ArgumentException("Попытка получить свойства объекта со значением null");
            }
            object lastValue = obj;
            T key = default;
            for (int i = 0; i < paths.Length; i++)
            {
                string member = paths[i];
                var arrayRegex = Regex.Matches(member, @"[(\d+)]");

                if (!arrayRegex.Any())
                {
                    key = keyGetter(lastValue.GetType(), member, flags);
                    lastValue = valueGetter(key, lastValue);
                }
                else
                {
                    string propName = member.Substring(0, arrayRegex.First().Index - 1);
                    key = keyGetter(lastValue.GetType(), propName, flags);
                    if (key == null)
                    {
                        throw new ArgumentException("Key is null");
                    }
                    var enumerable = valueGetter(key, lastValue) as IEnumerable<object>;
                    for (int j = 0; j < arrayRegex.Count - 1; j++)
                    {
                        if (!typeof(IEnumerable).IsAssignableFrom(typeGetter(key)))
                        {
                            throw new ArgumentException($"Неверный путь, {key.Name} не является массивом");
                        }
                        int index = int.Parse(arrayRegex[j].Value);
                        enumerable = enumerable.ElementAt(index) as IEnumerable<object>;
                    }
                    lastValue = enumerable.ElementAt(int.Parse(arrayRegex.Last().Value));
                }
                tree.AddLast(lastValue);
            }
            return (key, tree);
        }
        public static (T, LinkedList<object>) Get<T>(Func<Type, string, BindingFlags, T> keyGetter,
                                        Func<T, object, object> valueGetter,
                                        Func<T, Type> typeGetter,
                                        object obj,
                                        List<PathArtifact> path,
                                        BindingFlags flags)
        {
            var tree = new LinkedList<object>();
            tree.AddFirst(obj);
            if (path.Count == 0)
            {
                throw new ArgumentException("Неправильный путь до поля");
            }
            if (obj == null)
            {
                throw new ArgumentException("Попытка получить свойства объекта со значением null");
            }
            object lastValue = obj;
            T key = default;
            for (int i = 0; i < path.Count; i++)
            {
                var p = path[i];
                if (!p.IsIndex)
                {
                    key = keyGetter(lastValue.GetType(), p.Key, flags);
                    if (key == null) throw new ArgumentException($"Неправильный путь к объекту: {obj} {path}");
                    lastValue = valueGetter(key, lastValue);
                }
                else
                {
                    var enumerable = lastValue as IEnumerable<object>;
                    lastValue = enumerable.ElementAt(p.Index);
                }
                tree.AddLast(lastValue);
            }
            return (key, tree);
        }
        public static List<PathArtifact> ParsePath(string pathStr)
        {
            var path = new List<PathArtifact>();
            string[] paths = pathStr.Split(".");
            for (int i = 0; i < paths.Length; i++)
            {
                string member = paths[i];
                var indexRegex = Regex.Matches(member, @"[(\d+)]");

                if (!indexRegex.Any())
                {
                    path.Add(new PathArtifact(member));
                }
                else
                {
                    string propName = member.Substring(0, indexRegex.First().Index - 1);
                    path.Add(new PathArtifact(propName));

                    for (int j = 0; j < indexRegex.Count; j++)
                    {
                        path.Add(new PathArtifact(int.Parse(indexRegex[j].Value)));
                    }
                }
            }
            return path;
        }
        public static (FieldInfo, LinkedList<object>) GetFieldThroughProperties(object obj, string path, BindingFlags flags = DefaultFlags)
        {
            var parsedPath = ParsePath(path);
            if (parsedPath.Count > 1)
            {
                var last = parsedPath.Last();
                parsedPath.RemoveAt(parsedPath.Count - 1);
                var (property, tree) = GetProperty(obj, parsedPath, flags);
                var field = tree.Last.Value.GetType().GetField(last.Key, flags);
                tree.AddLast(field);
                return (field, tree);
            }
            return GetField(obj, parsedPath, flags);
        }
        const BindingFlags DefaultFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
        public static (FieldInfo, LinkedList<object>) GetField(object obj, string path, BindingFlags flags = DefaultFlags)
        {
            Func<Type, string, BindingFlags, FieldInfo> keyGetter = (_type, _path, _flags) => _type.GetField(_path, _flags);
            Func<FieldInfo, object, object> valueGetter = (_key, _val) => _key.GetValue(_val);
            Func<FieldInfo, Type> typeGetter = (_fi) => _fi.FieldType;
            return Get(keyGetter, valueGetter, typeGetter, obj, path, flags);
        }
        public static (PropertyInfo, LinkedList<object>) GetProperty(object obj, string path, BindingFlags flags = DefaultFlags)
        {
            Func<Type, string, BindingFlags, PropertyInfo> keyGetter = (_type, _path, _flags) => _type.GetProperty(_path, _flags);
            Func<PropertyInfo, object, object> valueGetter = (_key, _val) => _key.GetValue(_val);
            Func<PropertyInfo, Type> typeGetter = (_pi) => _pi.PropertyType;
            return Get(keyGetter, valueGetter, typeGetter, obj, path, flags);
        }
        public static (FieldInfo, LinkedList<object>) GetField(object obj, List<PathArtifact> path, BindingFlags flags = DefaultFlags)
        {
            Func<Type, string, BindingFlags, FieldInfo> keyGetter = (_type, _path, _flags) => _type.GetField(_path, _flags);
            Func<FieldInfo, object, object> valueGetter = (_key, _val) => _key.GetValue(_val);
            Func<FieldInfo, Type> typeGetter = (_fi) => _fi.FieldType;
            return Get(keyGetter, valueGetter, typeGetter, obj, path, flags);
        }
        public static (PropertyInfo, LinkedList<object>) GetProperty(object obj, List<PathArtifact> path, BindingFlags flags = DefaultFlags)
        {
            Func<Type, string, BindingFlags, PropertyInfo> keyGetter = (_type, _path, _flags) => _type.GetProperty(_path, _flags);
            Func<PropertyInfo, object, object> valueGetter = (_key, _val) => _key.GetValue(_val);
            Func<PropertyInfo, Type> typeGetter = (_pi) => _pi.PropertyType;
            return Get(keyGetter, valueGetter, typeGetter, obj, path, flags);
        }
        public static void SetPropertyValue(object target, string path, object newValue, BindingFlags flags = DefaultFlags)
        {
            var (property, tree) = GetProperty(target, path, flags);
            property.SetValue(tree.Last.Previous.Value, newValue);
        }
        public static void SetFieldValue(object target, string path, object newValue, BindingFlags flags = DefaultFlags)
        {
            var (field, tree) = GetField(target, path, flags);
            field.SetValue(tree.Last.Previous.Value, newValue);
        }
        public static object GetPropertyValue(object target, string path, BindingFlags flags = DefaultFlags)
        {
            var (property, tree) = GetProperty(target, path, flags);
            return tree.Last.Value;
        }
        public static object GetFieldValue(object target, string path, BindingFlags flags = DefaultFlags)
        {
            var (field, tree) = GetField(target, path, flags);
            return tree.Last.Value;
        }

        /// <summary>
        /// Returns the index of the start of the contents in a StringBuilder
        /// </summary>
        /// <param name="value">The string to find</param>
        /// <param name="startIndex">The starting index.</param>
        /// <param name="ignoreCase">if set to <c>true</c> it will ignore case</param>
        /// <returns></returns>
        public static int IndexOf(this StringBuilder sb, string value, int startIndex, bool ignoreCase)
        {
            int index;
            int length = value.Length;
            int maxSearchLength = sb.Length - length + 1;

            if (ignoreCase)
            {
                for (int i = startIndex; i < maxSearchLength; ++i)
                {
                    if (char.ToLower(sb[i]) == char.ToLower(value[0]))
                    {
                        index = 1;
                        while ((index < length) && (char.ToLower(sb[i + index]) == char.ToLower(value[index])))
                            ++index;

                        if (index == length)
                            return i;
                    }
                }

                return -1;
            }

            for (int i = startIndex; i < maxSearchLength; ++i)
            {
                if (sb[i] == value[0])
                {
                    index = 1;
                    while ((index < length) && (sb[i + index] == value[index]))
                        ++index;

                    if (index == length)
                        return i;
                }
            }

            return -1;
        }
    }
}
