using System;

namespace Rex.Core.Data
{
    /// <summary>
    /// Проверьте, что все поля, реализуемые свойствами имеют такое же название, что и поля, начиная со строчного символа
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreSyncAttribute : Attribute
    {
    }
}
