using System.Collections;

namespace Rex.Core.Data
{
    public interface ISyncableCollection
    {
        void AddRange(IList collection);
        void _AddRangeSilent(IList collection);
        void _AddSilent(object item);
        void _MoveSilent(int oldIndex, int newIndex);
        void _RemoveAtSilent(int index);
        void _RemoveSilent(object item);
        object this[int index] { get; set; }
    }
}
