using System;
using System.Linq.Expressions;

namespace Rex.Core.Data
{
    public static class NameOf
    {
        public static string Full<TSource>(Expression<Func<TSource, object>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            string result = memberExpression?.ToString();
            if (memberExpression == null)
            {
                if (expression.Body is UnaryExpression unaryExpression)
                {
                    result = unaryExpression.Operand.ToString();

                }
                else if (expression.Body is MethodCallExpression methodCallExpression)
                {
                    result = methodCallExpression.ToString();
                }
            }
            result = result.Substring(result.IndexOf('.') + 1).Replace("Call(", "").Replace(".get_Item(", "[").Replace(")", "]");
            return result;
        }
    }
}

