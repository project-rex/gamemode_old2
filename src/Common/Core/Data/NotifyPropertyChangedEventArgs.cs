using System;

namespace Rex.Core.Data
{
    public class NotifyPropertyChangedEventArgs : EventArgs
    {
        public NotifyPropertyChangedEventArgs(object newValue) => NewValue = newValue;

        public object NewValue { get; set; }
    }
}
