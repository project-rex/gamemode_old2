using System.Collections;
using _System.Collections.ObjectModel;
using _System.Collections.Specialized;

namespace Rex.Core.Data
{
    public class SyncableCollection<T> : ObservableCollection<T>, ISyncableCollection, INotifyEntityChangedBase
    {
        private bool supressNotification = false;

        object ISyncableCollection.this[int index] { get => base[index]; set => base[index] = (T)value; }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!supressNotification)
                base.OnCollectionChanged(e);
        }
        /// <summary>
        /// Не вызывает уведомление
        /// </summary>
        /// <param name="item"></param>
        public void _AddSilent(object item)
        {
            supressNotification = true;
            base.Add((T)item);
            supressNotification = false;
        }
        /// <summary>
        /// Не вызывает уведомление
        /// </summary>
        /// <param name="item"></param>
        public void _RemoveSilent(object item)
        {
            supressNotification = true;
            base.Remove((T)item);
            supressNotification = false;
        }
        /// <summary>
        /// Не вызывает уведомление
        /// </summary>
        /// <param name="index"></param>
        public void _RemoveAtSilent(int index)
        {
            supressNotification = true;
            base.RemoveAt(index);
            supressNotification = false;
        }
        public void _MoveSilent(int oldIndex, int newIndex)
        {
            supressNotification = true;
            base.Move(oldIndex, newIndex);
            supressNotification = false;
        }
        public void AddRange(IList collection)
        {
            supressNotification = true;
            foreach (T item in collection)
            {
                Add(item);
            }
            supressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        public void _AddRangeSilent(IList collection)
        {
            supressNotification = true;
            foreach (T item in collection)
            {
                Add(item);
            }
            supressNotification = false;
        }
    }
}
