namespace Rex.Core.Data
{
    public interface INotifyEntityChangedBase
    {
    }

    public abstract class NotifyPropertyChangedBase : INotifyEntityChangedBase
    {
        public NotifyPropertyChangedBase()
        {

        }
        public event EntityChangedEventHandler EntityChanged;

        public void Set<T>(ref T propertyRef, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
        {
            if (ReferenceEquals(propertyRef, value)) return;
            propertyRef = value;
            EntityChanged?.Invoke(this, new EntityChangedEventArgs(propertyName));
        }
    }
}
