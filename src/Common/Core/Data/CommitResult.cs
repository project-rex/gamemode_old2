using System.Collections.Generic;

namespace Rex.Core.Data
{
    public class CommitResult
    {
        public CommitResult(string realPath, string publicPath, LinkedList<object> target, object oldValue, object newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
            RealPath = realPath;
            PublicPath = publicPath;
            TargetTree = target;
        }

        public object OldValue { get; set; }
        public object NewValue { get; set; }
        public LinkedList<object> TargetTree { get; set; }
        public string RealPath { get; set; }
        public string PublicPath { get; set; }
    }
}
