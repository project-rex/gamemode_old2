using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using _System.Collections.Specialized;

namespace Rex.Core.Data
{
    public abstract class ChangeTracker : IDisposable
    {
        public event EntityChangedEventHandler EntityChanged;

        protected abstract void SubscribeAll();
        protected abstract void UnsubscribeAll();

        protected virtual void RaiseEntityChanged(object sender, EntityChangedEventArgs e)
        {
            EntityChanged?.Invoke(sender, e);
        }
        public static ChangeTracker Create(NotifyPropertyChangedBase observable)
        {
            if (observable is INotifyCollectionChanged)
            {
                return new ChildCollectionChangeTracker(observable as INotifyCollectionChanged, string.Empty);
            }
            return new ChildPropertyChangeTracker(observable, string.Empty, true);
        }

        public void Dispose()
        {
            UnsubscribeAll();
        }
    }
    public class ChildPropertyChangeTracker : ChangeTracker
    {
        protected readonly Dictionary<PropertyInfo, ChangeTracker> childTrackers = new Dictionary<PropertyInfo, ChangeTracker>();
        protected NotifyPropertyChangedBase _target { get; set; }
        private readonly string path;
        private readonly bool first = false;
        public ChildPropertyChangeTracker(NotifyPropertyChangedBase target, string path, bool first = false)
        {
            this.path = path;
            _target = target;
            this.first = first;
            _target.EntityChanged += new EntityChangedEventHandler(TargetChanged);
            SubscribeAll();
        }

        protected override void SubscribeAll()
        {
            var deps = _target
                .GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !p.GetCustomAttributes(typeof(IgnoreSyncAttribute), false).Any())
                .Where(p => typeof(NotifyPropertyChangedBase).IsAssignableFrom(p.PropertyType) || typeof(INotifyCollectionChanged).IsAssignableFrom(p.PropertyType));

            foreach (var dep in deps)
            {
                Subscribe(dep);
            }
        }
        protected override void UnsubscribeAll()
        {
            foreach (var property in childTrackers.Keys)
            {
                Unsubscribe(property);
            }
        }
        private void Subscribe(PropertyInfo property)
        {
            object value = property.GetValue(_target);
            if (value == null)
            {
                childTrackers[property] = null;
                return;
            }
            string path = first ? property.Name : this.path + "." + property.Name;
            if (value is INotifyCollectionChanged notifyCollectionChanged)
            {
                childTrackers[property] = new ChildCollectionChangeTracker(notifyCollectionChanged, path);
            }
            else if (value is NotifyPropertyChangedBase notifyPropertyChanged)
            {
                childTrackers[property] = new ChildPropertyChangeTracker(notifyPropertyChanged, path);
            }
            childTrackers[property].EntityChanged += new EntityChangedEventHandler(DepChanged);
        }
        private void Unsubscribe(PropertyInfo p)
        {
            var listener = childTrackers[p];
            if (listener == null)
            {
                return;
            }

            listener.EntityChanged -= new EntityChangedEventHandler(DepChanged);
            listener.Dispose();
        }

        public void DepChanged(object sender, EntityChangedEventArgs e)
        {
            RaiseEntityChanged(sender, e);
        }
        public void TargetChanged(object sender, EntityChangedEventArgs e)
        {
            // e.ChangeType = EntityChangeType.Property;
            var changedProperty = _target.GetType().GetProperty(e.Path, BindingFlags.Public | BindingFlags.Instance);
            e.Path = first ? e.Path : path + "." + e.Path;
            e.AdditionalArgs = new NotifyPropertyChangedEventArgs(changedProperty.GetValue(_target));
            RaiseEntityChanged(sender, e);
            if (childTrackers.Keys.Contains(changedProperty))
            {
                Unsubscribe(changedProperty);
                Subscribe(changedProperty);
            }
        }
    }

    internal class ChildCollectionChangeTracker : ChangeTracker
    {
        private readonly INotifyCollectionChanged _target;
        private readonly Dictionary<INotifyEntityChangedBase, ChangeTracker> _collectionListeners = new Dictionary<INotifyEntityChangedBase, ChangeTracker>();
        private readonly string _path;
        public ChildCollectionChangeTracker(INotifyCollectionChanged target, string path)
        {
            _path = path;
            _target = target;
            _target.CollectionChanged += TargetChanged;
            SubscribeAll();
        }

        protected override void SubscribeAll()
        {
            int i = 0;
            foreach (INotifyEntityChangedBase item in (IEnumerable)_target)
            {
                Subscribe(item, i);
                i++;
            }
        }
        protected override void UnsubscribeAll()
        {
            foreach (var item in _collectionListeners.Keys)
            {
                _collectionListeners[item].EntityChanged -= new EntityChangedEventHandler(DepChanged);
                _collectionListeners[item].Dispose();
            }
            _collectionListeners.Clear();

        }
        private void Subscribe(INotifyEntityChangedBase item, int index)
        {
            if (item is NotifyPropertyChangedBase entity)
            {
                if (_collectionListeners.ContainsKey(entity))
                {
                    return;
                }

                var listener = new ChildPropertyChangeTracker(entity, _path + $"[{index}]");
                listener.EntityChanged += new EntityChangedEventHandler(DepChanged);
                _collectionListeners.Add(entity, listener);
            }
            else if (item is INotifyCollectionChanged collection)
            {
                if (_collectionListeners.ContainsKey(item))
                {
                    return;
                }
                var listener = new ChildCollectionChangeTracker(collection, _path + $"[{index}]");
                listener.EntityChanged += new EntityChangedEventHandler(DepChanged);
                _collectionListeners.Add(item, listener);
            }
        }

        private void DepChanged(object sender, EntityChangedEventArgs e)
        {
            RaiseEntityChanged(sender, e);
        }

        private void Unsubscribe(INotifyEntityChangedBase item)
        {
            if (!_collectionListeners.ContainsKey(item)) return;
            var tracker = _collectionListeners[item];
            tracker.EntityChanged -= new EntityChangedEventHandler(DepChanged);
            tracker.Dispose();
            _collectionListeners.Remove(item);
        }


        private void TargetChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                UnsubscribeAll();
            }
            else
            {
                var args = new EntityChangedEventArgs(_path)
                {
                    AdditionalArgs = e,
                    // ChangeType = EntityChangeType.Collection
                };
                if (e.OldItems != null)
                {
                    foreach (INotifyEntityChangedBase item in e.OldItems)
                    {
                        Unsubscribe(item);
                    }
                }

                if (e.NewItems != null)
                {
                    int i = 0;
                    foreach (INotifyEntityChangedBase item in e.NewItems)
                    {
                        Subscribe(item, e.NewStartingIndex + i);
                        i++;
                    }
                }
                RaiseEntityChanged(sender, args);
            }
        }
    }
}
