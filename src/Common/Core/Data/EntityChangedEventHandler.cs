using System;

namespace Rex.Core.Data
{
    /// <summary>
    /// Represents the method that will handle the <see langword='PropertyChanged'/>
    /// event raised when a property is changed on a component.
    /// </summary>
    public delegate void EntityChangedEventHandler(object sender, EntityChangedEventArgs args);
}
