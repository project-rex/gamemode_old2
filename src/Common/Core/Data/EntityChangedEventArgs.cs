using System;

namespace Rex.Core.Data
{
    public class EntityChangedEventArgs : EventArgs
    {
        public string Path { get; set; }
        public EventArgs AdditionalArgs { get; set; }
        public EntityChangedEventArgs(string path)
        {
            Path = path;
        }
    }
}
