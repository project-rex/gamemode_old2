using System;
using System.Text;
using System.Text.RegularExpressions;
using _System.Collections.Specialized;
using Rex.Core.Misc;
namespace Rex.Core.Data
{
    public class DataController
    {
        readonly IDebugger debugger;

        public DataController(IDebugger debugger) => this.debugger = debugger;
        public DataController() { }

        public CommitResult CommitChange(object obj, EntityChangedEventArgs args)
        {
            var (property, tree) = Helpers.GetProperty(obj, args.Path);
            object value = tree.Last.Value;

            if (args.AdditionalArgs is NotifyCollectionChangedEventArgs collectionArgs)
            {
                var collection = value as ISyncableCollection;
                switch (collectionArgs.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        debugger?.DebugLog("Adding collectionArgs.OldStartingIndex " + collectionArgs.OldItems.Count + " elements");
                        collection._AddRangeSilent(collectionArgs.NewItems);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        debugger?.DebugLog($"Removing from: {collectionArgs.OldStartingIndex} {collectionArgs.OldItems.Count} elements");
                        for (int i = 0; i < collectionArgs.OldItems.Count; i++)
                        {
                            collection._RemoveAtSilent(collectionArgs.OldStartingIndex + i);
                        }
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        throw new NotSupportedException("Синхронизация NotifyCollectionChangedAction.Reset не реализована");
                    case NotifyCollectionChangedAction.Replace:
                        debugger?.DebugLog($"Replacing at: {args.Path}. Index: {collectionArgs.NewStartingIndex}. Old: {collectionArgs.OldItems[0]}. New: {collectionArgs.NewItems[0]}");
                        collection[collectionArgs.NewStartingIndex] = collectionArgs.NewItems[0];
                        break;
                    case NotifyCollectionChangedAction.Move:
                        collection._MoveSilent(collectionArgs.OldStartingIndex, collectionArgs.NewStartingIndex);
                        break;
                }
                return new CommitResult(args.Path, args.Path, tree, collectionArgs.OldItems, collectionArgs.NewItems);
            }
            else if (args.AdditionalArgs is NotifyPropertyChangedEventArgs propertyArgs)
            {
                string privatePath = PropertyPathToPrivateField(args.Path);
                if (property.PropertyType.IsEnum)
                {
                    propertyArgs.NewValue = Enum.ToObject(property.PropertyType, propertyArgs.NewValue);
                }
                var (field, _target) = Helpers.GetFieldThroughProperties(obj, privatePath);
                field.SetValue(_target.Last.Previous.Value, propertyArgs.NewValue);
                return new CommitResult(privatePath, args.Path, _target, value, propertyArgs.NewValue);
            }
            throw new ArgumentException("Неверный тип изменения: " + args.AdditionalArgs.GetType().Name);
        }
        public static string MakePathGeneric(string originalPath)
        {
            return Regex.Replace(originalPath, @"\[\d+\]", "[?]");
        }
        public static string PropertyPathToPrivateField(string propertyPath)
        {
            var sb = new StringBuilder(propertyPath);
            int lastDotIndex = propertyPath.LastIndexOf(".");
            sb[lastDotIndex + 1] = char.ToLower(sb[lastDotIndex + 1]);
            return sb.ToString();
        }
    }
}
