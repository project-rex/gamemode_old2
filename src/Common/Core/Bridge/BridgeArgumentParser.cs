using System;
using System.Collections.Generic;
using System.Reflection;
namespace Rex.Core.Bridge
{
    public static class BridgeArgumentParser
    {
        public static object[] ConvertArgsToParameters(object[] args, IList<ParameterInfo> parameterInfos)
        {
            if (args.Length != parameterInfos.Count)
            {
                throw new ArgumentException("Count of parameters mismatch!");
            }

            var result = new object[args.Length];
            for (int i = 0; i < args.Length; i++)
            {
                var type = args[i].GetType();
                if (type.IsAssignableFrom(parameterInfos[i].ParameterType) || type.IsValueType)
                {
                    result[i] = Convert.ChangeType(args[i], parameterInfos[i].ParameterType);
                }
                else
                {
                    result[i] = args[i];
                }
            }
            return result;
        }
    }
}
