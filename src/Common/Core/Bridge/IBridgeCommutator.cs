namespace Rex.Core.Bridge
{
    public interface IBridgeCommutator
    {
        void SendMessage(Packet message, object receiver);
    }
}
