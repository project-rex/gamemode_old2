using System.Reflection;

namespace Rex.Core.Bridge
{
    public class BridgeEventInfo
    {
        public EndPoint ExpectedDeparture { get; set; }
        public bool IsAsync { get; set; }
        public MethodInfo Callback { get; set; }
        public object Target { get; set; }
        public bool Optimize { get; set; } = true;
        public bool ParseParameters { get; set; } = true;
    }
}
