using System.Threading.Tasks;

namespace Rex.Core.Bridge
{
    public interface ICallbackInvoker
    {
        void InvokeAction(BridgeEventInfo eventInfo, object[] msgData, object sender);
        object InvokeHandler(BridgeEventInfo eventInfo, object[] msgData, object sender);
        Task<object> InvokeHandlerAsync(BridgeEventInfo eventInfo, object[] msgData, object sender);
    }
}
