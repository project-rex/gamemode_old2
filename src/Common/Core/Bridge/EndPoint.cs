using System;

namespace Rex.Core.Bridge
{
    [Flags]
    public enum EndPoint
    {
        Server,
        Client,
        CEFMain,
        CEFDevices
    }
}
