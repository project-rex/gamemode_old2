using System;
using System.Reflection;

using Rex.Core.Misc;

namespace Rex.Core.Bridge
{
    public static class BridgeEventRegistrator
    {
        public static void Register(object target, BridgeBase bridge)
        {
            foreach (var methodInfo in Helpers.GetTypeMethodsWithAttribute(target.GetType(), typeof(RexEventAttribute)))
            {
                var attr = (RexEventAttribute)methodInfo.GetCustomAttribute(typeof(RexEventAttribute));
                if (attr.From == bridge.CurrentEndPoint)
                {
                    throw new Exception("From property can't be the same as current endpoint!");
                }
                FastExpressionInvoker.CacheMethod(methodInfo);
                string name = attr.EventName ?? methodInfo.Name;
                var eventInfo = new BridgeEventInfo
                {
                    ExpectedDeparture = attr.From,
                    Callback = methodInfo,
                    Target = target
                };
                if (methodInfo.ReturnType == typeof(void))
                {
                    bridge.RegisterAction(name, eventInfo);
                }
                else if (methodInfo.ReturnType.IsSubclassOf(typeof(System.Threading.Tasks.Task)))
                {
                    eventInfo.IsAsync = true;
                    if (methodInfo.ReturnType.IsGenericType)
                    {
                        bridge.RegisterHandler(name, eventInfo);
                    }
                    else
                    {
                        bridge.RegisterAction(name, eventInfo);
                    }
                }
                else
                {
                    bridge.RegisterHandler(name, eventInfo);
                }
            }
        }
    }
}
