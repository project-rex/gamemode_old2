using System;

namespace Rex.Core.Bridge
{
    public class Packet : ICloneable
    {
        public EndPoint Destination { get; set; }
        public EndPoint Departure { get; set; }
        public long SendTimeTicks { get; set; } = DateTime.Now.Ticks;
        public object[] Data { get; set; }
        public string FunctionName { get; set; }
        public bool IsResponseAwaiting { get; set; }
        public string PrivateID { get; set; }
        public string ErrorMessage { get; set; }
        public Packet(EndPoint destination, EndPoint departure, string functionName, object[] data)
        {
            Destination = destination;
            Departure = departure;
            Data = data ?? Array.Empty<object>();
            FunctionName = functionName;
        }

        public void SwapEndPoints()
        {
            int _ = (int)Destination;
            Destination = Departure;
            Departure = (EndPoint)_;
        }

        public override string ToString()
        {
            return $"{Departure} -> {Destination} {FunctionName} AwaitingReponse: {IsResponseAwaiting}";
        }

        public string UniqueID()
        {
            return PrivateID + SendTimeTicks + FunctionName;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
