using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Rex.Core.Misc;

namespace Rex.Core.Bridge
{
    public abstract class BridgeBase
    {
        public abstract EndPoint CurrentEndPoint { get; }
        protected abstract ICallbackInvoker CBInvoker { get; }
        protected abstract IDebugger Logger { get; }


        private readonly Dictionary<string, BridgeEventInfo> _handlers = new Dictionary<string, BridgeEventInfo>();
        private readonly Dictionary<string, List<BridgeEventInfo>> _actions = new Dictionary<string, List<BridgeEventInfo>>();
        private readonly Dictionary<string, BridgeEventInfo> _pendingActions = new Dictionary<string, BridgeEventInfo>();

        protected abstract void SendMessage(Packet message, object receiver);

        protected void SendMessageWithPromisedResponse(Packet message, BridgeEventInfo info, object receiver)
        {
            string id = Guid.NewGuid().ToString();
            message.IsResponseAwaiting = true;
            message.PrivateID = id;

            if (_pendingActions.ContainsKey(id))
            {
                throw new Exception("Pending action with id " + id + " already registered");
            }
            _pendingActions.Add(id, info);
            SendMessage(message, receiver);
        }

        public void RegisterAction(string funcName, BridgeEventInfo info)
        {
            if (!_actions.ContainsKey(funcName))
            {
                _actions.Add(funcName, new List<BridgeEventInfo> { info });
            }
            else
            {
                _actions[funcName].Add(info);
            }
        }

        public void RegisterHandler(string funcName, BridgeEventInfo info)
        {
            if (_handlers.ContainsKey(funcName))
            {
                throw new Exception("Handler for function " + funcName + " already registered");
            }
            _handlers.Add(funcName, info);
        }
        protected virtual void InternalRequest(object receiver, EndPoint point, string fName, params object[] data)
        {
            var message = new Packet(point, CurrentEndPoint, fName, data);
            SendMessage(message, receiver);
        }
        protected void InternalRequest(object receiver, EndPoint point, string fName, Action<object> cb, params object[] data)
        {
            var message = new Packet(point, CurrentEndPoint, fName, data);
            FastExpressionInvoker.CacheMethod(cb.Method);
            var eventInfo = new BridgeEventInfo()
            {
                Callback = cb.Method,
                Target = cb.Target,
                ExpectedDeparture = point,
                IsAsync = false,
                Optimize = true,
                ParseParameters = true
            };
            SendMessageWithPromisedResponse(message, eventInfo, receiver);
        }
        protected virtual Task InternalRequestAsync(object receiver, EndPoint point, string fName, params object[] data)
        {
            return Task.Run(() => InternalRequest(receiver, point, fName, data));
        }

        protected virtual Task<T> InternalRequestAsync<T>(object receiver, EndPoint point, string fName, params object[] data)
        {
            var message = new Packet(point, CurrentEndPoint, fName, data);
            var completSource = new TaskCompletionSource<T>();
            var cb = CreateCBDelegate(completSource);
            var eventInfo = new BridgeEventInfo()
            {
                Callback = cb.Method,
                Target = cb.Target,
                ExpectedDeparture = point,
                IsAsync = false,
                Optimize = true
            };
            SendMessageWithPromisedResponse(message, eventInfo, receiver);
            return completSource.Task;
        }
        private delegate void AsyncActionCallbackDelegate<T>(T t);
        private AsyncActionCallbackDelegate<T> CreateCBDelegate<T>(TaskCompletionSource<T> tSource)
        {
            return (T t) => tSource?.TrySetResult(t);
        }
        public void OnMessageReceived(Packet message, object sender)
        {
            // message.RouteHistory.Add(CurrentEndPoint);
            if (message.Destination == CurrentEndPoint)
            {
                HandleRequest(message, sender);
            }
            else
            {
                SendMessage(message, sender);
            }
        }
        private void SendResponseMessage(Packet inMessage, object result, object receiver)
        {
            var responseMessage = (Packet)inMessage.Clone();
            responseMessage.Data = new object[] { result };
            responseMessage.IsResponseAwaiting = false;
            responseMessage.SwapEndPoints();
            SendMessage(responseMessage, receiver);
        }
        protected void HandleRequest(Packet inMessage, object sender)
        {
            // Отправитель ожидает ответа
            object[] msgData = inMessage.Data;
            if (inMessage.IsResponseAwaiting)
            {
                var eventInfo = GetHandlerByFunctionName(inMessage.FunctionName);
                if (inMessage.Departure != eventInfo.ExpectedDeparture)
                {
                    throw new Exception("Просьба дать ответ с того места, откуда не ждали! " + inMessage.FunctionName);
                }
                if (eventInfo.IsAsync)
                {
                    CBInvoker.InvokeHandlerAsync(eventInfo, msgData, sender).ContinueWith(completedTask =>
                      {
                          object result = null;
                          if (completedTask.Exception != null)
                          {
                              result = completedTask.Exception.InnerException;
                          }
                          else
                          {
                              inMessage.ErrorMessage = completedTask.Exception.InnerException?.Message ?? completedTask.Exception.Message;
                          }
                          SendResponseMessage(inMessage, result, sender);
                      });
                }
                else
                {
                    object result = null;
                    try
                    {
                        result = CBInvoker.InvokeHandler(eventInfo, msgData, sender);
                    }
                    catch (Exception e)
                    {
                        inMessage.ErrorMessage = e.Message;
                    }
                    SendResponseMessage(inMessage, result, sender);
                }
            }
            // Не ожидает ответа и нет ID - RPC запрос без ожидания ответа
            else if (string.IsNullOrEmpty(inMessage.PrivateID))
            {
                foreach (var actionEventInfo in GetActionsByFunctionName(inMessage.FunctionName))
                {
                    if (actionEventInfo.ExpectedDeparture == inMessage.Departure)
                    {
                        CBInvoker.InvokeAction(actionEventInfo, msgData, sender);
                    }
                    else
                    {
                        Logger.DebugWarn("warning: there are actions with exact name from another endpoint " + inMessage.FunctionName);
                    }
                }
            }
            // Есть ID и не нужен ответ - значит это и есть ответ
            else
            {
                var pendingActionInfo = PopPendingActionById(inMessage.PrivateID);
                CBInvoker.InvokeAction(pendingActionInfo, msgData, sender);
            }
        }
        private List<BridgeEventInfo> GetActionsByFunctionName(string funcName)
        {
            if (!_actions.ContainsKey(funcName))
            {
                throw new ArgumentException("Actions for function " + funcName + " were not registered!");
            }

            return _actions[funcName];
        }
        private BridgeEventInfo GetHandlerByFunctionName(string funcName)
        {
            if (!_handlers.ContainsKey(funcName))
            {
                throw new ArgumentException("Handler for function " + funcName + " was not registered!");
            }

            return _handlers[funcName];
        }
        private BridgeEventInfo PopPendingActionById(string id)
        {
            if (!_pendingActions.ContainsKey(id))
            {
                throw new ArgumentException("Pendingaction with id " + id + " was not registered!");
            }

            var pendingAction = _pendingActions[id];
            _pendingActions.Remove(id);
            return pendingAction;
        }
    }
}
