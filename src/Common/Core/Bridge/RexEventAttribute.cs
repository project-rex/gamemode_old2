using System;

namespace Rex.Core.Bridge
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class RexEventAttribute : Attribute
    {
        public string EventName { get; set; } = null;
        public EndPoint From { get; set; }
        public RexEventAttribute(EndPoint from)
        {
            From = from;
        }
        public RexEventAttribute(string eventName, EndPoint from)
        {
            EventName = eventName;
            From = from;
        }
    }
}
