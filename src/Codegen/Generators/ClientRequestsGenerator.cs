using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;

using Rex.Core.Bridge;
using Rex.Core.Misc;

namespace Rex.Codegen.Generators
{
    public class ClientRequestsGenerator : ICodeGenerator
    {
        public static string LowerizeFirstLetter(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            return s.Length == 1 ? s.ToLower() : s.Remove(1).ToLower() + s[1..];
        }
        public void GenerateCode(string outDir)
        {
            var typesWithRexEvents = Assembly
               .GetAssembly(typeof(Server.Core.RexScript))
               .GetTypes()
               .Where(t => Helpers.GetTypeMethodsWithAttribute(t, typeof(RexEventAttribute)).Any());
            foreach (var type in typesWithRexEvents)
            {
                var provider = CodeDomProvider.CreateProvider("cs");
                var compileUnit = new CodeCompileUnit();
                var globalNamespace = new CodeNamespace();
                globalNamespace.Imports.Add(new CodeNamespaceImport("Rex.Domain"));
                globalNamespace.Imports.Add(new CodeNamespaceImport("Rex.Core.Bridge"));
                var samples = new CodeNamespace("Rex.Client.Clients");
                compileUnit.Namespaces.Add(globalNamespace);
                compileUnit.Namespaces.Add(samples);

                string className = type.Name + "Client";
                var clientClassDef = new CodeTypeDeclaration(className)
                {
                    Attributes = MemberAttributes.Public
                };

                string bridgeClientFieldName = LowerizeFirstLetter(nameof(Client.Core.Bridge));

                var bridgeClientField = new CodeMemberField
                {
                    Attributes = MemberAttributes.Private,
                    Name = bridgeClientFieldName,
                    Type = new CodeTypeReference(typeof(Client.Core.Bridge))
                };
                clientClassDef.Members.Add(bridgeClientField);

                var constructor = new CodeConstructor
                {
                    Attributes =
                    MemberAttributes.Public | MemberAttributes.Final
                };
                // Add parameters.
                constructor.Parameters.Add(new CodeParameterDeclarationExpression(
                       typeof(Client.Core.Bridge), "_" + bridgeClientFieldName));

                // Add field initialization logic
                var bridgeClientRef =
                    new CodeFieldReferenceExpression(
                    new CodeThisReferenceExpression(), bridgeClientFieldName);
                constructor.Statements.Add(new CodeAssignStatement(bridgeClientRef,
                    new CodeArgumentReferenceExpression("_" + bridgeClientFieldName)));

                clientClassDef.Members.Add(constructor);
                foreach (var srcMethodInfo in Helpers.GetTypeMethodsWithAttribute(type, typeof(RexEventAttribute)))
                {
                    var attr = srcMethodInfo.GetCustomAttribute<RexEventAttribute>(true);
                    if (attr.From != EndPoint.Client)
                    {
                        continue;
                    }

                    if (srcMethodInfo.ReturnType.FullName.Contains(nameof(Server)))
                    {
                        throw new Exception($"Тип {srcMethodInfo.ReturnType.FullName} не может использоваться на клиенте!");
                    }

                    var methodDef = new CodeMemberMethod
                    {
                        Name = srcMethodInfo.Name + "Async",
                        Attributes = MemberAttributes.Public | MemberAttributes.Final
                    };
                    var srcMethodParams = srcMethodInfo.GetParameters().ToList();
                    if (srcMethodParams.Count > 0 && srcMethodParams[0].ParameterType == typeof(GTANetworkAPI.Player))
                    {
                        srcMethodParams.RemoveAt(0);
                    }

                    foreach (var srcMethodParam in srcMethodParams)
                    {
                        if (srcMethodParam.ParameterType.FullName.Contains(nameof(Server)))
                        {
                            throw new Exception($"Тип {srcMethodParam.ParameterType.FullName} не может использоваться на клиенте!");
                        }
                        methodDef.Parameters.Add(new CodeParameterDeclarationExpression(srcMethodParam.ParameterType, srcMethodParam.Name));
                    }
                    if (srcMethodInfo.ReturnType == typeof(void))
                    {
                        methodDef.ReturnType = new CodeTypeReference(typeof(void));
                    }
                    else
                    {
                        methodDef.ReturnType = new CodeTypeReference(typeof(Task<>).MakeGenericType(srcMethodInfo.ReturnType));
                    }

                    var bridgeArgs = new List<CodeExpression>()
                    .Append(new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(typeof(EndPoint)), "Server"))
                    .Append(new CodePrimitiveExpression(attr.EventName ?? srcMethodInfo.Name))
                    .ToList();
                    bridgeArgs.AddRange(srcMethodParams.Select(p => new CodeVariableReferenceExpression(p.Name)));

                    CodeMethodReferenceExpression codeMethodReferenceExpression;
                    if (srcMethodInfo.ReturnType == typeof(void) || srcMethodInfo.ReturnType == typeof(Task))
                    {
                        codeMethodReferenceExpression = new CodeMethodReferenceExpression(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), bridgeClientFieldName), nameof(Client.Core.Bridge.Request));
                    }
                    else
                    {
                        codeMethodReferenceExpression = new CodeMethodReferenceExpression(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), bridgeClientFieldName),
                         nameof(Client.Core.Bridge.RequestAsync),
                            new CodeTypeReference(srcMethodInfo.ReturnType));
                    }
                    var reqInvokeExpr = new CodeMethodInvokeExpression(codeMethodReferenceExpression, bridgeArgs.ToArray());
                    if (srcMethodInfo.ReturnType != typeof(void))
                    {
                        var returnStatement = new CodeMethodReturnStatement
                        {
                            Expression = reqInvokeExpr
                        };
                        methodDef.Statements.Add(returnStatement);
                    }
                    else
                    {
                        methodDef.Statements.Add(reqInvokeExpr);
                    }
                    clientClassDef.Members.Add(methodDef);
                }

                samples.Types.Add(clientClassDef);
                var sw = new StringWriter();

                provider.GenerateCodeFromCompileUnit(compileUnit, sw, new CodeGeneratorOptions()
                {
                    BlankLinesBetweenMembers = false,
                });
                File.WriteAllText(Path.Combine(outDir, className + ".cs"), sw.ToString());
            }
        }
    }
}

