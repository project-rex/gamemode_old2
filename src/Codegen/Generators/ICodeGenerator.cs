namespace Rex.Codegen.Generators
{
    public interface ICodeGenerator
    {
        void GenerateCode(string outDir);
    }
}
