using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Rex.Codegen.Generators
{
    public class DomainTypesListGenerator
    {
        public string GenerateDomainTypesClass()
        {
            //var @namespace = NamespaceDeclaration(ParseName(nameof(Domain.Core))).NormalizeWhitespace();
            //@namespace.AddUsings(UsingDirective(ParseName(nameof(System.Collections.Generic))));
            //var classDeclaration = ClassDeclaration("DomainTypesContainer");
            //classDeclaration.AddModifiers(Token(SyntaxKind.PublicKeyword)).AddModifiers(Token(SyntaxKind.StaticKeyword));
            //var variableDeclaration = VariableDeclaration(ArrayType(IdentifierName("Type"), SingletonList(ArrayRankSpecifier())))
            //         .AddVariables(VariableDeclarator("DomainTypes"));
            //var fieldDeclaration = FieldDeclaration(variableDeclaration)
            //      .AddModifiers(Token(SyntaxKind.PublicKeyword)).AddModifiers(Token(SyntaxKind.StaticKeyword));

            //classDeclaration = classDeclaration.AddMembers(fieldDeclaration);
            //@namespace = @namespace.AddMembers(classDeclaration);

            //// Normalize and get code as string.
            //var code = @namespace
            //    .NormalizeWhitespace()
            //    .ToFullString();
            //return code;
            var provider = CodeDomProvider.CreateProvider("cs");

            var compileUnit = new CodeCompileUnit();
            var samples = new CodeNamespace("Rex.Domain");
            compileUnit.Namespaces.Add(samples);
            var @class = new CodeTypeDeclaration("DomainTypesContainer")
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Static
            };
            var field = new CodeMemberField
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                Name = "DomainTypes",
                Type = new CodeTypeReference(typeof(Type[]))
            };

            var domainTypes = Assembly
               .GetAssembly(typeof(Core.Misc.KnownTypesBinder))
               .GetTypes();
            var allTypes =
            domainTypes
               .Where(x => !Attribute.IsDefined(x, typeof(CompilerGeneratedAttribute)))
               .Where(x => !x.IsInterface)
               .Where(x => !x.IsAbstract)
               .Where(x => !x.IsSealed)
               .Where(x => !x.IsSubclassOf(typeof(Attribute)))
               .Append(typeof(List<>))
               .Append(typeof(List<object>))
               .Append(typeof(Dictionary<,>))
               .Append(typeof(Dictionary<object, object>))
               .Append(typeof(HashSet<object>))
               .Append(typeof(LinkedList<object>))
               .Select(x => new CodeTypeOfExpression(x))
               .ToArray();
            field.InitExpression = new CodeArrayCreateExpression(
                new CodeTypeReference(typeof(Type)), allTypes);
            @class.Members.Add(field);
            samples.Types.Add(@class);
            var sw = new StringWriter();

            provider.GenerateCodeFromCompileUnit(compileUnit, sw, null);

            return sw.ToString();
        }
    }
}
