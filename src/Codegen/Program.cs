using System;
using System.IO;

using Rex.Codegen.Generators;

namespace Rex.Codegen
{
    class Program
    {
        static void Main()
        {
            var domainTypesListGenerator = new DomainTypesListGenerator();
            var clientRequestsGenerator = new ClientRequestsGenerator();
            clientRequestsGenerator.GenerateCode(Path.Combine(AppContext.BaseDirectory, "../../../../", "Client", "Clients"));
            File.WriteAllText(Path.Combine(AppContext.BaseDirectory, "../../../../", "Domain", "Misc", "DomainTypesContainer.cs"), domainTypesListGenerator.GenerateDomainTypesClass());
        }
    }
}
