using Rex.Core.Data;

namespace Rex.Tests
{
    public class A : NotifyPropertyChangedBase
    {
        private B b;
        public B B { get => b; set => Set(ref b, value); }

        private SyncableCollection<C> collection = new SyncableCollection<C>();
        public SyncableCollection<C> Collection { get => collection; set => Set(ref collection, value); }

    }
    public class B : NotifyPropertyChangedBase
    {
        private C c;
        public C C { get => c; set => Set(ref c, value); }

        private SyncableCollection<C> collection = new SyncableCollection<C>();
        public SyncableCollection<C> Collection { get => collection; set => Set(ref collection, value); }
    }
    public class C : NotifyPropertyChangedBase
    {
        public float d;
        public float D { get => d; set => Set(ref d, value); }

        private SyncableCollection<SyncableCollection<C>> nestedCollection = new SyncableCollection<SyncableCollection<C>>();
        public SyncableCollection<SyncableCollection<C>> NestedCollection { get => nestedCollection; set => Set(ref nestedCollection, value); }

        public S s;
        public S S { get => s; set => Set(ref s, value); }
    }

    public struct S
    {
        public float D { get; set; }
    }
}
