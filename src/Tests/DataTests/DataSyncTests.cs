using System;
using _System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using Rex.Core.Data;
using Rex.Core.Misc;
using Xunit;

namespace Rex.Tests
{
    public class DataSyncTests
    {

        A a = new A();
        Random r = new Random();
        [Fact]
        public void TestChangeTracker()
        {
            var a = new A();
            var tracker = ChangeTracker.Create(a);
            EntityChangedEventArgs lastCalledArgs = default;
            tracker.EntityChanged += (sender, args) =>
            {
                if (args.AdditionalArgs is NotifyCollectionChangedEventArgs coll)
                {
                    lastCalledArgs = args;
                }
                else if (args.AdditionalArgs is NotifyPropertyChangedEventArgs prop)
                {
                    lastCalledArgs = args;
                }
            };
            a.B = new B
            {
                C = new C()
            };
            Assert.Equal("B", lastCalledArgs.Path);
            var c = a.B.C;
            c.D = 15;
            Assert.Equal("B.C.D", lastCalledArgs.Path);

            a.Collection.Add(new C()
            {
                D = 228
            });
            Assert.Equal("Collection", lastCalledArgs.Path);
            Assert.True(lastCalledArgs.AdditionalArgs is NotifyCollectionChangedEventArgs);

            a.Collection[0].D = 500;
            Assert.Equal("Collection[0].D", lastCalledArgs.Path);

            a.B.Collection.Add(new C() { D = 63 });
            Assert.Equal("B.Collection", lastCalledArgs.Path);
            Assert.True(lastCalledArgs.AdditionalArgs is NotifyCollectionChangedEventArgs);

            a.B.Collection.RemoveAt(0);
            Assert.Equal("B.Collection", lastCalledArgs.Path);
            Assert.True(lastCalledArgs.AdditionalArgs is NotifyCollectionChangedEventArgs);
            Assert.Empty(a.B.Collection);

            a.B = null;
            Assert.Equal("B", lastCalledArgs.Path);
            Assert.True(lastCalledArgs.AdditionalArgs is NotifyPropertyChangedEventArgs);
            Assert.Null((lastCalledArgs.AdditionalArgs as NotifyPropertyChangedEventArgs).NewValue);

        }
        [Fact]
        public void TestPropertyGet()
        {
            a.Collection.Add(new C()
            {
                D = 228
            });
            a.B = new B();
            a.B.Collection.Add(new C()
            {
                D = 777
            });
            a.B.Collection.Add(new C()
            {
                D = 778
            });
            a.B.C = new C();

            a.B.C.NestedCollection.Add(new SyncableCollection<C>());
            a.B.C.NestedCollection[0].Add(new C()
            {
                D = 228f
            });
            var aCollection0D = Helpers.GetPropertyValue(a, NameOf.Full<A>(a => a.Collection[0].D));
            Assert.Equal(228f, aCollection0D);

            var aBcollection0D = Helpers.GetPropertyValue(a, NameOf.Full<A>(a => a.B.Collection[0].D));
            Assert.Equal(777f, aBcollection0D);


            var aBcollection1D = Helpers.GetPropertyValue(a, NameOf.Full<A>(a => a.B.Collection[1].D));
            Assert.Equal(778f, aBcollection1D);


            var aBCNestedCollection01 = Helpers.GetPropertyValue(a, NameOf.Full<A>(a => a.B.C.NestedCollection[0][0].D));
            Assert.Equal(228f, aBCNestedCollection01);

        }
        [Fact]
        public void TestPropertySet()
        {
            a.Collection.Add(new C()
            {
                D = 228
            });
            a.B = new B();
            a.B.Collection.Add(new C()
            {
                D = 777
            });
            a.B.Collection.Add(new C()
            {
                D = 778
            });
            a.B.C = new C();

            a.B.C.NestedCollection.Add(new SyncableCollection<C>(){
                new C(){
                    D = 228f
                }
            });
            a.B.C.NestedCollection[0].Add(new C()
            {
                D = 228f
            });

            string propertyName;
            object propertyValue;
            var r = new Random();
            object newValue;

            newValue = (float)r.NextDouble() * r.Next(1000);
            propertyName = NameOf.Full<A>(a => a.Collection[0].D);
            Helpers.SetPropertyValue(a, propertyName, newValue);
            propertyValue = Helpers.GetPropertyValue(a, propertyName);
            Assert.Equal(newValue, propertyValue);

            newValue = (float)r.NextDouble() * r.Next(1000);
            propertyName = NameOf.Full<A>(a => a.B.Collection[1].D);
            Helpers.SetPropertyValue(a, propertyName, newValue);
            propertyValue = Helpers.GetPropertyValue(a, propertyName);
            Assert.Equal(newValue, propertyValue);

            newValue = (float)r.NextDouble() * r.Next(1000);
            propertyName = NameOf.Full<A>(a => a.B.C.NestedCollection[0][0].D);
            Helpers.SetPropertyValue(a, propertyName, newValue);
            propertyValue = Helpers.GetPropertyValue(a, propertyName);
            Assert.Equal(newValue, propertyValue);

            newValue = (float)r.NextDouble() * r.Next(1000);
            propertyName = NameOf.Full<A>(a => a.B.C.NestedCollection[0][0].d);
            var (field, tree) = Helpers.GetFieldThroughProperties(a, propertyName);
            field.SetValue(tree.Last.Previous.Value, newValue);
            Assert.Equal(newValue, field.GetValue(tree.Last.Previous.Value));
        }
        [Fact]
        public void TestNameOf()
        {
            Assert.Equal("B.C.NestedCollection", NameOf.Full<A>(a => a.B.C.NestedCollection));
            Assert.Equal("B.C.D", NameOf.Full<A>(a => a.B.C.D));

            Assert.Equal("B.Collection[1]", NameOf.Full<A>(a => a.B.Collection[1]));
            Assert.Equal("Collection[228].D", NameOf.Full<A>(a => a.Collection[228].D));
            Assert.Equal("B.C.NestedCollection[1][5]", NameOf.Full<A>(a => a.B.C.NestedCollection[1][5]));
            Assert.Equal("B.C.NestedCollection[1][5].D", NameOf.Full<A>(a => a.B.C.NestedCollection[1][5].D));
            Assert.Equal("B.C.NestedCollection[1][5].NestedCollection[7][1]", NameOf.Full<A>(a => a.B.C.NestedCollection[1][5].NestedCollection[7][1]));
        }
        [Fact]
        public void GenericPathTest()
        {
            Assert.Equal("A.B.C[?]", DataController.MakePathGeneric("A.B.C[0]"));
            Assert.Equal("A.B[?][?].C[?]", DataController.MakePathGeneric("A.B[775][2].C[0]"));
            Assert.Equal("A.B[?][?].C[?].F", DataController.MakePathGeneric("A.B[775][2].C[0].F"));
        }
        [Fact]
        public void DataControllerTest()
        {
            a = new A()
            {
                Collection = new SyncableCollection<C>(){
                    new C()
                },
                B = new B()
                {
                    C = new C()
                    {
                        NestedCollection = new SyncableCollection<SyncableCollection<C>>()
                        {
                            new SyncableCollection<C>(){
                                new C(){
                                    D = 5f
                                }
                            }
                        }
                    },
                    Collection = new SyncableCollection<C>()
                    {
                        new C()
                    }
                }
            };
            float newValue;
            var shadowA = JObject.FromObject(a).ToObject<A>();
            Assert.NotEqual(a, shadowA);
            Assert.False(ReferenceEquals(a, shadowA));
            var changeTracker = ChangeTracker.Create(a);
            var controller = new DataController();
            changeTracker.EntityChanged += (sender, args) =>
            {
                controller.CommitChange(shadowA, args);
            };
            Assert.Single(a.Collection);
            a.Collection.Add(new C()
            {
                D = 5
            });
            Assert.Equal(2, a.Collection.Count);
            Assert.Equal(a.Collection.Count, shadowA.Collection.Count);
            Assert.Equal(a.Collection[1].D, shadowA.Collection[1].D);

            newValue = (float)r.NextDouble() * r.Next(1000);
            a.Collection[1].D = newValue;
            Assert.Equal(newValue, a.Collection[1].D);
            Assert.Equal(newValue, shadowA.Collection[1].D);

            // тест нелогичный, так как при коммите устанавливаются ссылочные значения
            // var newValue2 = (float)r.NextDouble() * r.Next(1000);
            // a.Collection[1]._d = newValue2;
            // Assert.False(ReferenceEquals(a.Collection[1], shadowA.Collection[1]));
            // Assert.Equal(newValue2, a.Collection[1].D);
            // Assert.Equal(newValue, shadowA.Collection[1].D);

            newValue = (float)r.NextDouble() * r.Next(1000);
            a.B.C.NestedCollection[0][0].D = newValue;

            Assert.Equal(newValue, a.B.C.NestedCollection[0][0].D);
            Assert.Equal(newValue, shadowA.B.C.NestedCollection[0][0].D);

            newValue = (float)r.NextDouble() * r.Next(1000);

            var newElement = new SyncableCollection<C>(){
                new C(){
                    D = newValue
                }
            };
            a.B.C.NestedCollection.Add(newElement);
            a.B.C.NestedCollection = JArray.FromObject(a.B.C.NestedCollection).ToObject<SyncableCollection<SyncableCollection<C>>>();
            newValue = (float)r.NextDouble() * r.Next(1000);
            a.B.C.NestedCollection[1][0].D = newValue;
            Assert.Equal(a.B.C.NestedCollection[1][0].D, shadowA.B.C.NestedCollection[1][0].D);

            newValue = (float)r.NextDouble() * r.Next(1000);
            a.B.C.NestedCollection[1][0].S = new S()
            {
                D = newValue
            };
            newValue = (float)r.NextDouble() * r.Next(1000);
            shadowA.B.C.NestedCollection[1][0].S = new S()
            {
                D = newValue
            };
            a.B.C.NestedCollection[1][0].S = new S()
            {
                D = 5f
            };
            Assert.Equal(a.B.C.NestedCollection[1][0].S.D, shadowA.B.C.NestedCollection[1][0].S.D);

        }
        [Fact]
        public void DataControllerStressTest()
        {
            a = new A()
            {
                Collection = new SyncableCollection<C>()
            };
            var changeTracker = ChangeTracker.Create(a);
            var controller = new DataController();
            var shadowA = JObject.FromObject(a).ToObject<A>();
            changeTracker.EntityChanged += (sender, args) => controller.CommitChange(shadowA, args);
            const int times = 10000;
            var c = new C()
            {
                D = (float)r.NextDouble() * r.Next(10000)
            };
            for (int i = 0; i < times; i++)
            {
                a.Collection.Add(c);
            }
            Assert.Equal(times, shadowA.Collection.Count);
            for (int i = times - 1; i > -1; i--)
            {
                a.Collection.RemoveAt(i);
            }
            Assert.Equal(a.Collection.Count, shadowA.Collection.Count);

            for (int i = 0; i < times; i++)
            {
                a.Collection.Add(new C()
                {
                    D = 5f
                });
            }
            Assert.Equal(times, shadowA.Collection.Count);
            for (int i = times - 1; i > -1; i--)
            {
                a.Collection.RemoveAt(i);
            }
            Assert.Equal(a.Collection.Count, shadowA.Collection.Count);
        }
        [Fact]
        public void TestMakePathGeneric()
        {
            Assert.Equal("A.B.C[?][?].D._d[?]", DataController.MakePathGeneric("A.B.C[4][555].D._d[1]"));
            Assert.Equal("A.B.C[?].d[?].d5", DataController.MakePathGeneric("A.B.C[4].d[1].d5"));
        }
        [Fact]
        public void TestPropertyPathToPrivateField()
        {
            Assert.Equal("A.B.C[4][555].D.d", DataController.PropertyPathToPrivateField("A.B.C[4][555].D.D"));
            Assert.Equal("A.B.C[4][555].D.D[7].f", DataController.PropertyPathToPrivateField("A.B.C[4][555].D.D[7].F"));
            Assert.Equal("a", DataController.PropertyPathToPrivateField("A"));
            Assert.Equal("a.B.c", DataController.PropertyPathToPrivateField("a.B.C"));
            Assert.Equal("a.B.testVar", DataController.PropertyPathToPrivateField("a.B.TestVar"));
        }
        [Fact]
        public void TestParsePath()
        {
            var path = Helpers.ParsePath(NameOf.Full<A>(a => a.B.C.NestedCollection[1][5]));
            Assert.Equal(5, path.Count);

            Assert.Equal("B", path[0].Key);
            Assert.False(path[0].IsIndex);

            Assert.Equal("C", path[1].Key);
            Assert.False(path[1].IsIndex);

            Assert.Equal("NestedCollection", path[2].Key);
            Assert.False(path[2].IsIndex);

            Assert.Equal(1, path[3].Index);
            Assert.True(path[3].IsIndex);

            Assert.Equal(5, path[4].Index);
            Assert.True(path[4].IsIndex);


            path = Helpers.ParsePath(NameOf.Full<A>(a => a.Collection[1].NestedCollection));
            Assert.Equal(3, path.Count);

            Assert.Equal("Collection", path[0].Key);

            Assert.Equal(1, path[1].Index);
            Assert.True(path[1].IsIndex);

            Assert.Equal("NestedCollection", path[2].Key);
            Assert.False(path[2].IsIndex);
        }
    }
}
